import java.lang.reflect.*;
import java.util.*;

class Dummy {
    int num;
    public int getNum(){
        return num;
    }

    public void setNum(int num){
        this.num = num;
    }

    public static String method(){
        return "45";
    }

    public void show(){
        System.out.println("Num is  " + num);
    }
}

class Main {
    public static void main(String[] args) {
        Dummy d = new Dummy();
        d.show();
        Map<String, Method> a = new HashMap<>();
        for(Method m : d.getClass().getDeclaredMethods()){
            a.put(m.getName(), m);
            System.out.println(m.getName());
        }

        try{
            Method setter = a.get("setNum");
            Method stat = a.get("method");
            Method show = a.get("show");
            Method valueOf = Integer.class.getMethod("valueOf", String.class);
            Class[] returnType = setter.getParameterTypes();

            System.out.println(returnType[0] + returnType[0].getName() );
            setter.invoke(d,
                            valueOf.invoke(null,
                                                stat.invoke(null)
                                           )
                        );
            show.invoke(d);

        }catch (IllegalAccessException e){
            System.out.println("fbjk");
        }catch (InvocationTargetException e){
            System.out.println("Invok");
        }
        catch(NoSuchMethodException e){
            System.out.println("No method found");
        }

    }
}
