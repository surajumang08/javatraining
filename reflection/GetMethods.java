/*
        Reflection can give you the ability to find out class info at run time.
*/
package reflection;
import java.lang.reflect.*;

public class GetMethods {
        public static void main(String[] args) {

                try {
                        Class clazz = Class.forName(args[0]);

                        for(Method m : clazz.getMethods()){
                                System.out.println(m.getName());
                        }

                }
                catch (Exception e) {

                }

        }
}
