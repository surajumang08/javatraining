

package generics;
import generics.classes.*;
interface A{

}

public class OnlyOneClass {
      static final int i;

      static{
        System.out.println(OnlyOneClass.i);
      }
        public static void main(String[] args) throws Exception {
                String name = "Suraj";
                Integer i = new Integer(455);
                Box<String> bs = new Box<String>("Hello");
                Box<Integer> bi = new Box<Integer>(123);

                System.out.println(String.class == String.class);
                System.out.println(bs.getClass().getName());
                System.out.println(bi.getClass().getName());
                System.out.println(bs.getClass() == bi.getClass());

                System.out.println(A.class.getName());
                A.class.newInstance();

        }
}
