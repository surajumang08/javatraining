/*

*/
package generics;

public class GenericMethod {

        public static void main(String[] args) {
                new GenericMethod().add( 45 , 6);
        }

        public <T extends Number> int add(T a, T b) {
                return a.intValue() + b.intValue();
        }
}
