/*
    Wildcard with extends is used as shown below.

    Is it possible to use it like this???
    List<? extends Animal> animals = new ArraList<>();

    A method which wishes to accept a List of any of the Animals can
    be declared as
    void method (List <? extends Animal>);

    Can it be used as a return type like
    List<? extends Animal> method();

    As a method argument this can accept a List of any Animal(including
    subclasses). With this power comes some restrictions.
    The items in the List are nothing but references of type Animal which
    can refer to any subtype of Animal.

        1   This method can't modify the object reference pointed by
            the List items. (Reasons) ?????
*/
package generics;
import java.util.*;
import classesandinterfaces.*;

public class WildcardWithExtends {
    /*
        Addition of new object to this list is not allowed. Why??
        This list parameter using Wildcard with extends 'classname' can be assigned any
        list(with a typename which is a subtype of 'classname').

        So a list<? extends Animal> can be assigned an actual list of Dogs,
        Cats, Rabbits and so on.

        Then if a method accepting List<? extends Animal> tries to add an animals
        like Fox in the list, It may result in an error. because the actual List
        may be referring to a list of Dogs which can't accept a Fox.
    */
    public void show(List<? extends Fruit> mangoes) {
        for (int i=0; i<mangoes.size() ;i++ ){
            mangoes.get(i).eat();
            //mangoes.add(new Mango("Nasfo"));
            //not even fruit can be added.
        }
    }

    public static void main(String[] args) {

        List<Mango> fruits = new ArrayList<>();
        fruits.add(new Mango("Suraj"));
        fruits.add(new Alphonse("Sujit"));
        fruits.add(new Mango("Rahul"));

        WildcardWithSuper w = new WildcardWithSuper();

        w.show(fruits);
    }
}
