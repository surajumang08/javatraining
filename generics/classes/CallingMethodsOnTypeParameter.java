/*

*/
package generics.classes;
import classesandinterfaces.*;

class Generic <T> {
    T a;
    T b;
    Generic(T a, T b) {
        this.a = a;
        this.b = b;
    }
    public void call(){
        System.out.println(a.equals(b));
    }
}

public class CallingMethodsOnTypeParameter {
    public static void main(String[] args) {
        Generic<Fruit> gf = new Generic<>(new classesandinterfaces.Apple("HelloWorld"),
                                        new classesandinterfaces.Apple("Helloworld"));
        gf.call();
    }
}
