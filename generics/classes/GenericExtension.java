/*

*/
package generics.classes;

public class GenericExtension {

    public static void main(String[] args) {
        // A1<Integer> a = new A1<>();
        // A<String> s = new A<>();
    }
}
/*
    the type parameter T extends Number and implements the interface A and B
*/
class ObjectContainer <T extends Number & Comparable & java.io.Serializable > {

}
