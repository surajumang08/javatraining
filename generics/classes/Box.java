/*

*/
package generics.classes;
import java.util.*;
import classesandinterfaces.*;


public class Box <Tt>  {
        Tt item;

        public Box(){
                /* instantiation in case of generic type parameter is not possible.
                 It is the reason why ArrayList uses an Object array.
                 But why is this instantiation not possible.
                */


        }
        public Box(Tt a){
                item = a;
        }

        // public <T> void show(ArrayList<T> a){
        //
        // }

        // public void print(){
        //         for(int i = 0; i < item.size(); i++)
        //                 System.out.println(item.get(i));
        // }
        //
        public <U extends Tt> void set(U item){
                this.item = item;
        }

        public Tt get() {
                return this.item;
        }
}

class Bounded <T> {


        public static void main(String[] args) {
                Bounded bb = new Bounded();
                Number n = bb.add(45, new Float(45.0));
                Number k = bb.add(45l, new Double(12));

                System.out.println(n.getClass().getName());
                System.out.println(k.getClass().getName());

        }
        public void method (List<? extends Fruit> f){

        }
        public void method2(){
                List<?> as = new ArrayList<Apple>();
                List<? extends Apple> bs = new ArrayList<Apple>();
                List<? super Apple> cs = new ArrayList<Apple>();
                //method(as);
                method(bs);
                method3(cs);

        }
        public void method3(List<? super GoldenDel> b){
                b.add(new GoldenDel("hello"));
                String s = (String)null;
                System.out.println(s);

        }
        public <T> void method4(Collection<T> a){

        }

        public <T extends Number & java.io.Serializable> T add(T a, T b){
                System.out.println(a.getClass().getName());
                System.out.println(a.getClass().getName());
                return a;
        }
}
