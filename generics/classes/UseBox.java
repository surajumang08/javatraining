/*

*/

package generics.classes;

public class UseBox {
        public static void main(String[] args) {
                Box<String> boxOfStrings = new Box<>();
                Box<Integer> boxOfInteger = new Box<>();

                boxOfStrings.add(new String("Hello"));
                boxOfStrings.add("World");

                boxOfInteger.add(new Integer(1));
                boxOfInteger.add(45);

                Integer i = boxOfInteger.get(0);
                String s = boxOfStrings.get(1);

        }
}
