/*
    This demonstrates that the collection classes allows polymorphism at
    only the base type NOT on the generic type parameter.

    List <String> names = new ArraList<String>()
    Base type is polymorphic as ArraList instance is being assigned to a List
    reference.

    List <Fruit> fruits = new ArraList<Apple>()
    The above statement is an example of polymorphic type parameter which means
    that a Fruit's reference can be used to refer to an Apple's instance.
    But this is not allowed in case of generics.

    Even though the List reference can hold any objects which are subclass of
    Animal.
        eg: fruits.add(new Apple());
            fruits.add(new Mango());

    ** The type parameter must always match, so If they always have to be same
    the second type parameter is now optional(Java 8) and can be inferred
    from the first type parameter.
    This is also called as automatic type inference. **

    In other words ...
    Another restriction is that a List<Fruit> can only refer to a List(or it's
    subclasses) of Fruit and nothing else. Neither Apple nor Mango.

    The reason for this is type erasure. The types in <> are only visible to
    the compiler and they are not present in the byptcode.

    So the JVM never sees those parameters in <> brackets.
    They are meant only for the use of compiler using which it can enforce
    type safety(The primary reason why generics were developed).

    What does type safety actually mean??
*/

package generics;
import classesandinterfaces.*;

public class PolymorphicType {
    /*
        A non generic class can have a generic method.
    */
    public static void main(String[] args) {
        Alphonse a = new Mango();
    }
}
