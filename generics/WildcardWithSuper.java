/*

*/

package generics;
import java.util.*;
import classesandinterfaces.*;

public class WildcardWithSuper {

    public void show(List<? super Alphonse> mangoes) {

        mangoes.add(new Alphonse("Newer added"));
        mangoes.add(new Alphonse("a new mango"));

        for (int i=0; i<mangoes.size() ;i++ ) {
            // Fruit f = (Fruit) mangoes.get(i);
            // f.eat();
        }
    }

    public static void main(String[] args) {
        /*
            This list holds references of type Fruit. So it's elements can refer
            to any Object which is a subtype of Fruit.
        */
        List<Fruit> fruits = new ArrayList<>();
        fruits.add(new Mango("Suraj"));
        fruits.add(new Alphonse("Sujit"));

        fruits.add(new Apple("Applr"));

        WildcardWithSuper w = new WildcardWithSuper();

        w.show(fruits);
    }
}
