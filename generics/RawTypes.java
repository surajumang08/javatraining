/*
    The legacy code using
        List l = new ArraList();
    is also referred as Raw type.
    But this is not the same as List<Object> = new ArraList <Object>();
    Because the Latter can refer only to a List of (references of Type Object).
    But the former can refer to any List.
*/

public class RawType {
    
}
