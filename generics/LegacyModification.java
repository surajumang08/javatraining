/*
    This class demonstrate how using a generic collection and the legacy
    version can cause problems.
*/

package generics;

import static collections.FruitCollection.*;
import classesandinterfaces.*;
import java.util.*;

public class LegacyModification {
    public static void main(String[] args) {
        //Apple<String> apples = new Apple<>();
        List<Fruit> fruits = new ArrayList<>(fruitMap.values());
        LegacyModification l = new LegacyModification();

        print(fruits);
        l.legacyMethod(fruits);

        print(fruits);

    }

    /*
        Accepts a generic type List of Apple but accepts it in a legacy container
        So compile time type safety is no longer applicable.
        Hence the compiler has no control on what this collection might add.
        Type safety is no longer guranteed.
    */
    public void legacyMethod(List fruits) {
        fruits.add(new String("HelloWorld"));
    }
}
