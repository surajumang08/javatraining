public class First {
  int field1;
  long field2;

  First(int field1, long field2) {
    this.field1 = field1;
    this.field2 = field2;
  }

  void callMe() {
    Second s = new Second(field1, field2);
  }

}
