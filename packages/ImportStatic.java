/*
    Static imports are a way to imports all the Static members of a
    given class.

    If we wish to import all the static members of a class then we will
    need to use the star in=import after the name of the class.

    Otherwise we can import simgle methods using this
    eg import static java.lang.Math.sqrt;
    this will import only the sqrt method from Math.

    To import all the static methods we can do
    import static java.lang.Math.*;
*/

import static java.math.BigDecimal;

public class ImportStatic {
    public static void main(String[] args) {
        System.out.println(longDigitLength(452L));
    }
}
