package classesandinterfaces;

public class GoldenDel extends Apple {

    public GoldenDel(String name){
        super(name);
    }

    public void eat(){
        System.out.println("Eating Apple " + name);
    }
}
