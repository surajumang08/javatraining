
package classesandinterfaces;

public class Mango extends Fruit {

    public Mango(){

    }
    public Mango(String name) {
        super(name);
    }

    public void eat(){
        System.out.println("Eating Mango");
    }

    public void mangoSpecific (){
        System.out.println("Mango Specific");
    }

}
