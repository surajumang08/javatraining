
package classesandinterfaces;

import java.lang.annotation.*;
import java.lang.reflect.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface NameCheck {
    String pattern() default "dfg";
}
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface DateCheck{
    String pattern() default "dfg";
}
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface EmailCheck{
    String pattern() default "dfg";
}
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface NumberCheck {
    String pattern() default "dfg";
}

public class ClassLiteral {

    public static void main(String[] args) {
        String str = "Hello";
        System.out.println("Before loop");
        Dummy d = new Dummy();

        System.out.println(Namecheck.class);
        System.out.println(DateCheck.class);
        
        for(Method m : Dummy.class.getMethods()){
            for(Annotation a : m.getDeclaredAnnotations()){
                System.out.println(a);
                ValidatorFactory.getInstance(a.getClass());
                check(NameCheck.class);
            }

        }
    }

    public static boolean check(Class<? extends Annotation> t){
        return t == Namecheck.class;
    }
}
