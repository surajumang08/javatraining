/*

*/
package classesandinterfaces;

public class Fruit implements Comparable<Fruit> {
    public String name;

    public Fruit(){
        //System.out.println(" Fruit's Constructor");
    }
    public Fruit(String name){
        this.name = name;
    }

    public String getName(){
        return name ;
    }

    public int compareTo(Fruit f){
        return this.getName().compareTo(f.getName());
    }

    public void setName(String name){
        this.name = name;
    }

    public void eat(){
        System.out.println("Eating Fruit");
    }

    public void fruitSpecific(){
        System.out.println("Fruit specific");
    }

    public int hashCode(){
        return this.getName().hashCode();
    }

    public boolean equals(Object o){
        return (o instanceof Fruit) ? ((Fruit)o).getName() == this.getName() : false ;
    }

    public String toString(){
        return this.name ;
    }
}
