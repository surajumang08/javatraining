
package classesandinterfaces;

public class Apple extends Fruit {

    {
        // Instance initialization block are run before constructor invokation
        // for every object being created.
        // but they are run only after call to super class constructor is finished.

        /*
            Actually the code inside Instance initialization block is copied in the
            beginning of all the constructors.

        System.out.println("Apple's initialization Block");
        */
    }

    public Apple(String name){
        super(name);
        //System.out.println("Apple's constructor");
    }

    public Apple(){
        //System.out.println("Apple's Ctor");
    }

    public void eat(){
        System.out.println("Eating Apple" + name);
    }

    public void appleSpecific(){
        System.out.println("Apple specific");
    }

    // @Override
    // public boolean equals(Object o) {
    //     // System.out.println(this.getClass().getName());
    //     // System.out.println(o.getClass().getName());
    //
    //     return (this.getClass().getName() == o.getClass().getName())
    //             && (this.getName() == ((Apple)o).getName() );
    // }

}
