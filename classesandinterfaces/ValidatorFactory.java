
package classesandinterfaces;

import java.lang.annotation.Annotation;

public class ValidatorFactory {
    //private static Validator v = null;

    public static void getInstance(Class<? extends Annotation> t){

        if (t == DateCheck.class)
            System.out.println("Date");
        if (t == EmailCheck.class)
            System.out.println("Email");
        if (t == NameCheck.class)
            System.out.println("Name");
        if (t == NumberCheck.class)
            System.out.println("Number");
        System.out.println("None");
    }
}
