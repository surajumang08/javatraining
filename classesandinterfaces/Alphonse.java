
package classesandinterfaces;

public class Alphonse extends Mango{

    public Alphonse(){

    }

    public Alphonse(String name){
        super(name);
    }

    public void eat(){
        System.out.println("Eating Alphonse");
    }

    public void alphonseSpecific(){
        System.out.println("Alphonse Specific");
    }
}
