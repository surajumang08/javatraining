
package classesandinterfaces.nestedclasses;
class Outer {
    public static void main(String[] args) {
        System.out.println("Hello");
    }

    /*
        Nested StaticNested class.
        Can create as many objects as you want.
        It is just a static member  of the enclosing class.
        There is no such thing as a static class.
        It is for namespace resolution.
        Every access modifier with abstract and final can be used.
        
    */

    protected final static class Nested {

    }
}

public class StaticNested {
    public static void main(String[] args) {
        // Outer.Nested o1 = new Outer.Nested();
        //
        // Outer.Nested o2 = new Outer.Nested();
        // System.out.println(o1 + " " + o2);
    }
}
