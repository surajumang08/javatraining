package classesandinterfaces.nestedclasses;

class Outer {

    private String firstName;
    private static String lastName;

    public void show(){
        System.out.println(firstName + " " + lastName);
    }

    Outer(){

    }

    Outer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    class InnerClass {
        private String firstName;
        private String lastName;

        private String onlyMine;

        InnerClass(){

        }

        InnerClass(String firstName, String lastName, String onlyMine){
            this.firstName = firstName;
            this.lastName = lastName;
            this.onlyMine = onlyMine;
        }

        public void show(){
            System.out.println(firstName + " " + lastName + " " + onlyMine);
        }

        public void parentShow(){
            System.out.println(Outer.this.firstName + " " + Outer.this.lastName);
        }
    }
}

public class InnerClass {
    public static void main(String[] args) {
        Outer o = new Outer("Suraj", "Kumar");
        Outer.InnerClass i = o.new InnerClass("Sujit", "Kumar", "Deepak");
        Outer.InnerClass j = o.new InnerClass("Sulekha", "Kumari", "Babiya");

        o.show();
        i.show();
        j.show();
        o = null;
        for (int ii = 0; ii<1000 ; ii++ ) {
            System.gc();
        }

        i.parentShow();
        j.parentShow();
    }
}
