/*
    Showing difference between overriding and redefining.

*/

/*
    Overloading static method with non static method.
    Is it Possible ?

*/
class Parent {
    protected void callMe(int a){
        System.out.println("Parent's callMe");
    }

    protected static void callMe() {
        System.out.println("Parent's static");
    }

    public void notMe() {
        System.out.println("Parent's not Me");
    }
}

public class AccessParentMethodUsingSuper extends Parent {

    private int val = 452;
    @Override
    public void callMe(int a) {
        System.out.println("child's callMe");
    }

    public static void callMe() {
        System.out.println("Child's Static");
    }

    public static void main(String[] args) {
        Parent.callMe(); //parent's static
        AccessParentMethodUsingSuper.callMe(); //child's static

        AccessParentMethodUsingSuper a = new AccessParentMethodUsingSuper();
        // a.callMe(); //parent's static
        // a.callMe(12); //child's callMe

        a.callMe(12); //parent's callMe
        System.out.println(a.val);
    }
}
