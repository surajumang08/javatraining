/*

*/
package overriding;
class Parent {
    Parent(){
        call();
    }
    public void call(){
        System.out.println("Parent's");
    }
}

public class CallOverriddenMethodInCtor extends Parent {
    int a = 100;
    CallOverriddenMethodInCtor(){
        call();
    }
    public void call(){
        System.out.println("Child's " + a);
    }
    public static void main(String[] args) {

        new CallOverriddenMethodInCtor();
    }
}
