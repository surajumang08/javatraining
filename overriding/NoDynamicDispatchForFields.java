

class Parent {
    public String S = "A";
}

public class NoDynamicDispatchForFields extends Parent {
    public String S = "B";

    public static void main(String[] args) {
        Parent p = new NoDynamicDispatchForFields();

        System.out.println(p.S);
    }
}
