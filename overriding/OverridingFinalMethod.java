public class OverridingFinalMethod {
  public final void Show(){
    System.out.println("Hello");
  }
}

class Second extends OverridingFinalMethod {
  public final void Show(){
    System.out.println("World");
  }
}
