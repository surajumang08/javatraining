/*
        A dummy class which will serve as an object which will be attached
        to the context object such that all the servlets are able to access
        it.

        The assignment will be done through a listener which will be
        implementing the ServContextListener and will be listening to the
        ServletContextEvent event.
*/
package com.example.model;

public class Apple {
        String name;

        public Apple(){
                name = "Default Apple";
        }
        public Apple(String name){
                this.name = name;
        }
        public String getName(){
                return name;
        }
        public String toString(){
                return "Apple is " + name;
        }
}
