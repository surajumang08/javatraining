
package com.example.web;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class AnotherServlet extends HttpServlet {
        public void doGet(HttpServletRequest request,
                HttpServletResponse response) throws IOException {

                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                
        }

        public void doPost(HttpServletRequest request,
                HttpServletResponse response) throws IOException, ServletException{

                doGet(request, response);
        }

        public static void staticMethod(){

        }

}
