/*
        This will demonstrate how a redirect is achieved.
*/
package com.example.web.dipatcher;

import com.example.model.Apple;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class Redirect extends HttpServlet {
        public void doGet(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {
                

        }
        public void doPost(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {

                doGet(request, response);
        }
}
