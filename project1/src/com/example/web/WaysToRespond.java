/*
        It will show what other ways are there to respond to a request.
        In other words what are the ways using which we can change the
        HttpServletResponse object.

        Writing and reading using the character stream or the Bibary stream.
        PrintWriter is a character stream.
        ServletOutputStream can also be used.

        Similarly for reading the BufferedReader can be used for reading the
        stream as characters while the ServletInputStream can be used for
        reading the stream as binary.

        Sending a static resource to the client i.e a pdf file or a jar file can
        be accomplished by using the binary stream and setting the MIME type
        (content-type) appropriately.
*/
