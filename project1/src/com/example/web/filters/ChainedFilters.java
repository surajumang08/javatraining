/*
        Implement multiple filters by chaining them together.

        chain.doFilter() achieves the chaining process.
        Since filters are also web components it is possible for them to
        dispatch a request.

        They use some wrapper to wrap the request and response objects such that
        the control comes back to them when the servlet has done it's work.

        They can also change the response object before it is sent back to the 
        client.
*/
