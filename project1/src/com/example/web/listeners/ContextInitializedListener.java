/*
        It will create a listener class which will be listening to the
        contextinitialized Event and when it recieves a notification then
        it will create an object and pass it inside the ServletContext
        object so that it will be accessible to all the web components.
*/
package com.example.web.listeners;
import com.example.model.Apple;
import javax.servlet.*;
import javax.servlet.http.*;

public class ContextInitializedListener implements ServletContextListener {

        public void contextInitialized(ServletContextEvent sce) {
                /*
                        It is having access to the ServletContext object
                        so it can add attribute to it.
                */
                sce.getServletContext().setAttribute("apple", new Apple("GoldenApple"));
        }

        public void contextDestroyed(ServletContextEvent sce) {

        }
}
