/*

        ServletConfig and GenericServlet both return an instance of
        ServletContext object.

        Some of the methods are.
                getInitParameter()
                getInitParameterNames()
                getAttribute()
                setAttribute(String, Object)
                removeAttribute(String)

*/
