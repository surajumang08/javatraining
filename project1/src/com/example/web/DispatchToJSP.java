/*
        Any request to a servlet can be dipatched to another web component by
        the servlet if it is not able to complete the request.

        RequestDispatcher class is having two mehthods
                forward()
                include()
        forward() is used to delegate the request to another web component which
        can be another servlet or JSP or any [static resource].

        purpose of include() ?

        This example will delegate the request to a JSP which will display the
        content.
*/

package com.example.web;
import com.example.model.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;


public class DispatchToJSP extends HttpServlet {

        public void doGet(HttpServletRequest request,
                HttpServletResponse response) throws IOException {

                /*
                        It is possible to attach some other attribute to the
                        request or response object and then pass it to the
                        JSP which will be able to access that data.

                        Normally the servlet will call a Plain java class for
                        the actual data which will be computed using the
                        business logic which has been separated from it.

                        And when the servlet gets the actual data it simply passes
                        it to the view so that it can be displayed.
                */
                request.setAttribute("added", "HelloWorld");
                getServletContext().setAttribute("New", "NewlyBorn");

                RequestDispatcher rd = request.getRequestDispatcher("result.jsp");
                try {
                        rd.forward(request, response);
                } catch(Exception e) {

                }

        }

        public void doPost(HttpServletRequest request,
                HttpServletResponse response) throws IOException, ServletException{

                doGet(request, response);
        }
}
