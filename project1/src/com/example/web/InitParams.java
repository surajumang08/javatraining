/*
        one can't set the values of a parameter because they are populated by
        the container from the deployment descriptor at the time of startup.

        request
*/

package com.example.web;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;


public class InitParams extends HttpServlet {
        public void doGet(HttpServletRequest request,
                HttpServletResponse response) throws IOException {

                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                /*
                        To get the parameters which were loaded by the container
                        at the time of startup.
                        The container creates a ServletConfig object and stores
                        the values mentioned in the web.xml DD in the form of
                        name value pair.
                        getServletConfig() is defined in javax.http.HttpServlet
                        and since this class extends the class so it is
                        available.

                        the init(ServletConfig) method defined in Servlet class
                        is implemented in HttpServlet.

                        The ServletConfig object is available only to one servlet
                        instance.

                */

                // the following is defined in GenericServlet class.
                ServletConfig config = getServletConfig();
                String firstName = config.getInitParameter("firstName");
                String lastName = config.getInitParameter("lastName");
                String pan = config.getInitParameter("PAN");
                out.print("First<br>");
                out.println(firstName + "<br>" + lastName + "<br>" + pan
                                        + "<br>");

                // out.flush();
                /*
                        Try to access the InitParams using Enumeration object.

                */
                Enumeration<String> e = config.getInitParameterNames();
                while(e.hasMoreElements()){
                        String pname = e.nextElement();
                        out.print("<br><B>" + pname + "<B>"
                                + config.getInitParameter(pname) + "<br>");
                }
                /*
                        Try printing the values of the context parameters which
                        are of type String.
                        parameters of type object are also possible and will
                        be added by the listener classes.
                */
                out.print("Third<br>");
                ServletContext context = getServletContext();
                e = context.getInitParameterNames();
                while(e.hasMoreElements()){
                        String pname = e.nextElement();
                        out.print("<br><B>" + pname + "<B>"
                                + context.getInitParameter(pname)+"<br>");
                }

        }

        public void doPost(HttpServletRequest request,
                HttpServletResponse response) throws IOException, ServletException{

                doGet(request, response);
        }


}
