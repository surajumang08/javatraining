/*
        A session object is created by calling the getSession() method.
        The session object is accessible throught the session.

        How does a session end? It is usually through a timeout field which
        is present inside the Cookie.

        How does a URL rewrite session end?

*/

package com.example.web.scopes;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;


public class SessionScope extends HttpServlet {
        public void doGet(HttpServletRequest request,
                HttpServletResponse response) throws IOException {

                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
        }
}
