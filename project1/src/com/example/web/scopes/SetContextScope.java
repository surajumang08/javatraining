/*
ContextScope can also be called as a Application Scope and it is the
widest of all the scopes. Any object present in this scope is accessible
to all the web components present in the Application.

The entries are in the name/value form where the name is a String and
value is an Object.

This can be used to provide objects which will be created at the time of
Application startup and will be available to every component.
Since it is a shared resource it is not thread safe.

The problem : If I need to store a custom object in the context scope
at the startup. How will my code know that the Application is starting?

The solution to this problem is using listeners which will be intimated
of the startup event also called as contextinitialized(). The method
opposite of this is contextdestroyed().

These two methods are called at the time of Application startup and
destruction.

Objects has to be created through a Listner class which must implement
the ServletContextListener interface which extends listener.

*/

package com.example.web.scopes;

import com.example.model.Apple;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

/*
        Set an attribute of the ServletContext object at the startup by listening
        to the contextinitialized event.

        Also try to access the object in more than one servlet.
*/

public class SetContextScope implements ServletContextListener {

        public void contextInitialized(ServletContextEvent sce) {
                ServletContext context = sce.getServletContext();
                Apple ap = new Apple(context.getInitParameter("appleName"));
                context.setAttribute("apple", ap);
                context.setAttribute("another", new Apple("yellow"));
        }

        public void contextDestroyed(ServletContextEvent sce) {

        }
}
