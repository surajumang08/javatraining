package com.example.web.scopes;

import com.example.model.Apple;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class AccessContextScope extends HttpServlet {
        public void doGet(HttpServletRequest request,
                HttpServletResponse response) throws IOException {

                response.setContentType("text/html");
                PrintWriter out = response.getWriter();

                ServletContext context = getServletContext();

                out.print(context.getInitParameter("lastName"));
                Apple ap = (Apple)context.getAttribute("apple");
                out.print("<br>" + ap + "<br>");

        }

        public void doPost(HttpServletRequest request,
                HttpServletResponse response) throws IOException, ServletException{

                doGet(request, response);
        }

}
