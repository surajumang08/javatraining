
package com.example.web;
import com.example.model.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;


public class BeerServlet extends HttpServlet {
        public void doGet(HttpServletRequest request,
                HttpServletResponse response) throws IOException {

                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                String c = request.getParameter("color");
                out.println("The beer color chosen by you <br>");

                List<String> brands = BeerExpert.getBrands(c);
                for (String s : brands ) {
                        out.println("<br> " + s);
                }

                /*
                        Dispatch reqest to another sservlet or try include()
                        on rd.
                */

        }

        public void doPost(HttpServletRequest request,
                HttpServletResponse response) throws IOException, ServletException{

                doGet(request, response);
        }


}
