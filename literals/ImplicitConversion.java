/*
    If a type higher than the passed type is available then an implicit
    conversion takes place.
    Same is true for object as well.
*/
package literals;

public class ImplicitConversion {

    public void call(float f){
        System.out.println("called with " + f);
    }

    public void call(long[] f) {
        System.out.println("Called with " + f);
    }

    public static void main(String[] args) {
        ImplicitConversion i = new ImplicitConversion();
        i.call(45);
        //i.call(new int[] {4, 5});
    }
}
