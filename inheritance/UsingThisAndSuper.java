public class UsingThisAndSuper {

  int a = 11, b = 12;
  String s;

  UsingThisAndSuper(){

  }

  UsingThisAndSuper(int a, int b){
    this.a = a;
    this.b = b;
  }

  UsingThisAndSuper(int a, String s){
    this.a = a;
    this.s = s;
    this.b = 1500;
  }
}

class ChildClass extends UsingThisAndSuper {

  int myValue;

  public static int Call(int num){
    return num;
  }
    // Instance block is execcuted before any constructor for each new object being created
  {
    myValue = 1200;
  }

  // Arguments to super() or this() can't be instance variable or instance method.
  // static variables and static  methods can be Arguments to super() and this()
  // constructors(or this(), super() ) can only be invoked from constructors
  ChildClass() {
    //super();
    this("Rahul") ;
  }
  ChildClass(String s){
    this.s = s;
  }

  public void Print(){
    System.out.println(a + " " + b + " " + s);
  }

  public static void main(String[] args) {
    ChildClass c =new ChildClass();
    c.Print();
  }
}
