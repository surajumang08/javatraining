/*

*/
package inheritance;
import classesandinterfaces.*;

public class InvalidCast extends Apple {
    InvalidCast(){

    }
    public void go(){
        System.out.println("GO invoked");
        go2(new Apple(), new InvalidCast());
        go2((InvalidCast)new Apple(), new InvalidCast() );
    }
    public void go2 (Apple a, InvalidCast i){
        System.out.println("go2 invoked");
        InvalidCast ii = (InvalidCast) a;
        Apple aa = (Apple)i;
    }

    public static void main(String[] args) {
        new InvalidCast().go();
    }
}
