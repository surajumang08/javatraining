public interface TrainedDog {

  String name = "Dog is Trained";
  default String trainer() {
    return "Dog Trainer";
  }
}
