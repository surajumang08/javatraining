/*
    Downcast example
    Fruit
      /\
     /  \
    /    \
Apple   Mango
            \
             \
              \
           Alphonse
*/
class Fruit {
    public void callme(){
        System.out.println("Fruit");
    }

    private void noneDown(){
        System.out.println("Fruit only");
    }
}

class Apple extends Fruit {
    public void callme(){
        System.out.println("Apple");
    }
}

class Mango extends Fruit {
    public void callme(){
        System.out.println("Mango");
    }
}

class Alphonse extends Mango {
    public void callme() {
        System.out.println("Alphonse");
    }
    // specific to alphonsw
    public void isJuicy(){
        System.out.println("Alphonse is Juicy");
    }
}

public class ErrorWithUpcast {
    public static void main(String[] args) {
        Alphonse m = new Alphonse();
        Fruit f = m;
        // f can only call methods in Fruit. Compilers checks
    }
}
