/*
    Downcast example
    Fruit
      /\
     /  \
    /    \
Apple   Mango
            \
             \
              \
           Alphonse
*/
class Fruit {
    public void callme(){
        System.out.println("Fruit");
    }

    private void noneDown(){
        System.out.println("Fruit only");
    }
}

class Apple extends Fruit {
    public void callme(){
        System.out.println("Apple");
    }
}

class Mango extends Fruit {
    public void callme(){
        System.out.println("Mango");
    }
}

class Alphonse extends Mango {
    public void callme() {
        System.out.println("Alphonse");
    }
    // specific to alphonsw
    public void isJuicy(){
        System.out.println("Alphonse is Juicy");
    }
}

public class DowncastExample {
    public static void main(String[] args) {
        Fruit a = new Alphonse();
        // cast alphonse
        System.out.println("Hello");
        Apple a1 = (Apple)a;
    }
}
