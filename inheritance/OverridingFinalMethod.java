// https://youtu.be/5V56DiadYoU

class Parent {

  public final int Name =120;
  public final void Show() {
      System.out.println(Name);
  }

  public void Print(){
    System.out.println(this.Name);
  }
}

public class OverridingFinalMethod extends Parent {

  public int Name = 450;

  public final void Show(int a){
    System.out.println(Name);
  }

  public static void main(String[] args) {
    OverridingFinalMethod s = new OverridingFinalMethod();
    s.Show(45);
    s.Print();
  }
}
