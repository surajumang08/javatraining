/*
    Upcast example
            Fruit
              /\
             /  \
            /    \
        Apple   Mango
*/

class Fruit {
    public void callme(){
        System.out.println("Fruit");
    }

    private void noneDown(){
        System.out.println("Fruit only");
    }
}

class Apple extends Fruit {
    public void callme(){
        System.out.println("Apple");
    }
}

class Mango extends Fruit {
    public void callme(){
        System.out.println("Mango");
    }
}

/*
    Upcasting will never call the parent's method if the chold has
    overridden that
*/
public class UpcastExample {
    public static void main(String[] args) {
        Fruit a = new Apple();

        a.callme();
        
    }
}
