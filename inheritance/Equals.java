
package inheritance;

import classesandinterfaces.*;

public class Equals {
    public static void main(String[] args) {
        Fruit a = new Apple("Hello");
        Fruit m = new Mango("Hello");

        System.out.println(m.equals(a));
    }
}
