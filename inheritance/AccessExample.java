package inheritance;

class Parent {
    private int s = 452;
    public void show(){
        System.out.println(s);
    }
}

public class AccessExample extends  Parent{
    public String s = "Deep";
    public static void main(String[] args) {
        AccessExample a = new AccessExample();
        a.show();
    }

}
