/*
    What happens when you define a method in a Subclass having the same
    signature as a static method in Parent.
*/

class Parent {
    public static void callMe(){
        System.out.println("In Parent");
    }
    // private static StaticMethodInheritance
    private static void priv(){
        System.out.println("Private methods Parent");
    }
}

public class StaticMethodInheritance extends Parent {
    public static void callMe(){
        System.out.println("In child");
    }

    private static void priv(){
        System.out.println("Private methods Child");
    }

    public static void main(String[] args) {
        StaticMethodInheritance p = new StaticMethodInheritance();
        p.callMe();     //Parent's
        ((Parent)p).callMe(); // child's

        // calling the private static methods
        p.priv();   // fine Child's version.
        ((Parent)p).priv(); //cTE

    }
}
