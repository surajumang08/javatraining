/*
    WHat happens !!
*/

class Parent {
    static String s = " ";
    protected Parent () {
        s += "Alpha";
    }
}

class Child extends Parent {
    protected Child() {
        s += "Sub";
    }
}

public class InheritingStaticMembers extends Child {

    private InheritingStaticMembers() {
        s += "SubSub";
    }
    public static void main(String[] args) {
        System.out.println(InheritingStaticMembers.s);
        System.out.println(Parent.s); //legal
        System.out.println(Child.s);
    }
}
