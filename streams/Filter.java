/*
    Demonstrates the stream. filter( java.util.function.Predicate<> )
    1   filter, map, flatmap, distinct, peek, limit, skip, builder
        empty, of, iterate, generate, concat,

    I'll be implementing all these methods with good use cases.
    
    Filter syntax is as follows

    filter is a method defined in the interface stream<T> which takes a Predicate
    of <? extends T> and returns a Stream<T>.
*/

package streams;
