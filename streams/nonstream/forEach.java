/*
        These will not return Stream<T> as their return types.
        1       mapTo<primitive type>
        2       forEach, forEachOrdered,
        3       toArray, reduce, collect, min, max
        4       anyMatch, noneMatch, allMatch
        5       findFirst, findAny
*/
