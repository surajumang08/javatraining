package package2;
import package1.MethodWithDefaultAccess;

public class DefaultModifier extends MethodWithDefaultAccess {

  DefaultModifier(){
      super("Saloni Mathur");
  }

  public static void main(String[] args) {
    MethodWithDefaultAccess d =new DefaultModifier();

    d.Show();
  }

}
