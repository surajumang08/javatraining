/*
    This class extends a public class in another package and extends
    [value and callMethod2 .]
    Constructors are not inherited(Never).
*/

package package2;
import package1.StaticProtectedMembers;
/*
    Since this class is in a package different from it's parent
    so it is only able to inherit the protected members of the parent.

    1: This code checks whether the static protected members of the parent
        can be accessed directly using the className or not.
    2: It is clear that protected members can't be accessed through
        an object created here.(However the protected members can be
        accessed through an object of the subclass).

    3: If an object of the parent class(or a parent reference to a child object)
       is created then only the the public members of the parent class
       can be accessed.

       Parent p = new Parent()
       Parent p = new Child()
       then only the public members can be accessed using the reference.

       Any call to a protected method using the parent's instance will
       result in a CTE.

       Dynamic method dispatch will only be available to the public methods
       in this scenario.
*/

public class TestStaticProtectedMembers extends StaticProtectedMembers {
    // Tring to access the static protected members from the parent.

    static int childValue = StaticProtectedMembers.callMethod();
    String childName = StaticProtectedMembers.Name;

    //Inherited method callMethod from parent.
    // Overload the inherited method.


    public static void main(String[] args) {
        TestStaticProtectedMembers t = new TestStaticProtectedMembers();
        /*
            Through this object all the protected and public methods and fields
            can be accessed.
        */
        System.out.println(childValue + " " + t.childName + " " + t.callMethod2());
        // Expected 4321 Suraj 1234
    }
}

/*
    Trying to access static fields and methods from non child class outside
    package.

    [Should not be accessible to non child classes outside package.]
*/

class NonChild {
    public static void main(String[] args) {
        // System.out.println(StaticProtectedMembers.callMethod() + " " +
        //                     StaticProtectedMembers.Name);
    }
}
