package package2;

import package1.ConstructorWithDefaultAccessInOtherPackage;

public class ConstructorTester extends ConstructorWithDefaultAccessInOtherPackage {

    ConstructorTester(){
      super("Suraj Kumar");
    }
    public static void main(String[] args) {
      ConstructorTester c = new ConstructorTester();
      c.Show();
    }
}
