package package1;

public class ConstructorWithDefaultAccessInOtherPackage {
  public String name = "Package1";

  public ConstructorWithDefaultAccessInOtherPackage(String name){
      this.name = name;
  }

  protected void Show(){
    System.out.println(name);
  }

}
