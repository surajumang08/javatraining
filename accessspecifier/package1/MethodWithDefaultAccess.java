package package1;
public class MethodWithDefaultAccess {
  String name = "Rahul Jangir";

  protected MethodWithDefaultAccess(String name) {
    this.name = name;
  }

  protected void Show(){
    System.out.println(name);
  }
}
