package package1;

public class StaticProtectedMembers {
    /*
        Only instance methods and fields are inherited and static
        methods and fields are only visible in the child class
    */
    protected int value = 100;
    protected static String Name = "Suraj Kumar";

    protected static int callMethod() {
        return 4321;
    }

    protected int callMethod2(){
        return 1234;
    }

    protected StaticProtectedMembers(){

    }
}
