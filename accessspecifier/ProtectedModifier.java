public class ProtectedModifier {
  protected int a = 100;
  protected String b = "Suraj";

  protected ProtectedModifier() {

  }

}

class SubClass extends ProtectedModifier{
  public static void main(String[] args) {
    ProtectedModifier p = new ProtectedModifier();

    System.out.println(p.a + " " + p.b);
  }
}
