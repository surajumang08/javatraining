package strings;

public class SubstringTest  {
    public static void main(String[] args) {
        String fieldName = "setFirstName".substring(3);
        System.out.println(fieldName);
        fieldName = fieldName.substring(0,1).toLowerCase() + fieldName.substring(1);
        System.out.println(fieldName);
    }
}
