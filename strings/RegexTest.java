
package strings;
import java.util.*;
public class RegexTest {
    public static void main(String[] args) {
        String phonePattern = "[0-9]{10}";
        String emailPattern = "(\\w)+@([A-Za-z]+\\.?)+";
        String datePattern  = "\\d{2}/\\d{2}/\\d{4}";
        String namePattern  = "[A-Za-z]+";
        String dotPattern   = "(\\w+\\.?)+";

        for(String str : args){
            if(str.matches(phonePattern))
                System.out.println(str + "--> phone");
            if(str.matches(emailPattern))
                System.out.println(str + "--> email");
            if(str.matches(namePattern))
                System.out.println(str + "--> name");
            if(str.matches(dotPattern))
                System.out.println(str + "--> dot");
            if(str.matches(datePattern))
                System.out.println(str + "--> date");

        }
    }
}
