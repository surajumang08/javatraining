/*
    What is the order for execution of instance variable initialization at
    declarion, Init Blocks, Constructors.
    Before every constructor invokation the init blocks and the declaration
    initialization will run from top to bottom in that order.

    Actually the value of init blocks and the declaration initialization
    are put inside the constructor body in the same top down order as they
    appear.

    Decompiled code for this is enclosed
    InstanceDefaults()
    {
        a = 1223;
        a = 500;
        a = 12;
        a = 2333;
    }
*/
package constructors;
public class InstanceDefaults {

    {
        a = 1223;
    }
    InstanceDefaults(){
        a = 2333;
    }
    {
        a = 500;
    }
    int a = 12;
    public static void main(String[] args) {
        System.out.println(new InstanceDefaults().a);
    }

}
