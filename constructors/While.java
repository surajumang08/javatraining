
package constructors;
public class While {

    void call(long a, Long b) {
        System.out.println("First");
    }
    void call(Long a, long b) {
        System.out.println("Second");
    }
    public static void main(String[] args) {
        While w = new While();
        /*
            A capital L implicitly wrappes into a Long.
        */
        w.call(5, 5l);   //First equivalent to w.call(5, 5L);
        w.call(new Long(5), 5L);  //Second equivalent to w.call(5L, 5);
    }
}
