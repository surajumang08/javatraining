import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;

class Person{
  int id;
  String firstName;
  String lastName;

  {
    firstName = "Suraj";
    lastName = "Kumar";
  }

  Person(int id, String firstName){
    this.id = id;
    this.firstName = firstName;
  }
  Person(int id, String firstName, String lastName){
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
  }



  public static void main(String[] args) {
    Person p1 = new Person(100, "Ramul");
    Person p2 = new Person(120, "Raloni", "Mathur");
    Person p3 = new Person(1, "Gaurav", "Sharma");

    List<Person> plist = new ArrayList<Person>();

    plist.add(p1);
    plist.add(p2);
    plist.add(p3);


    // Collections.sort(plist, new Comparator<Person>(){
    //   public int compare(Person p1, Person p2){
    //     // return 1 0 or -1
    //     return (p1.id != p2.id) ? (p1.id < p2.id)? -1 : 1 : 0;
    //   }
    // });

    // Collections.sort(plist, (p11, p22) -> (p11.id != p22.id) ? (p11.id < p22.id)? -1 : 1 : 0 );
    Collections.sort(plist, (p11, p22) -> p11.firstName.compareTo(p22.firstName));
      plist.forEach(System.out :: print);


  }

  public String toString(){
    return id + " " + firstName + " " + lastName;
  }
}
