public class StringPool {

  String s1 = "ME";
  String s2 = new String("ME");
  String s3 = new String("ME");
  String s4 = "ME";
  public static void main(String[] args) {
    StringPool s1 = new StringPool();
    System.out.println(s1.s1.hashCode() + " " + s1.s2.hashCode() + " "+ s1.s3.hashCode());

    System.out.println(s1.s2 == s1.s3); //both heap
    System.out.println(s1.s1 == s1.s4); // true both StringPool
    System.out.println(s1.s1 == s1.s2); // heap and pool
  }
}
