class IntLiteral {
  public static void main(String[] args) {
    Integer i = Integer.valueOf(12);
    Integer j = new Integer(12);

    Integer k = Integer.valueOf(145);
    Integer l = new Integer(145);

    System.out.println(i.hashCode() + " " + j.hashCode());
    System.out.println( i == j);
    System.out.println(k == l);
  }
}
