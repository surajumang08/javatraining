package exceptionhandling;

public class Questions {
    /*
        Declared that it throws a checked exception but
        Actually doesn't throw it.
    */
    void call(int type) throws java.io.IOException, java.io.FileNotFoundException{
        if(type == 1)
            throw new java.io.IOException();
        throw new java.io.FileNotFoundException();
    }
    /*
        Calling a method which throws an exception which is a parent(wider)
        of the exception the calling method has Declared to throw.
        Gives a CTE as "unreported exception must be caught or Declared to
        be thrown"

        Even when a parent and a child exception appears in the throws clause
        there is no CTE.
        But if a child exception is thrown then what ????

    */
    void caller() throws java.io.IOException {
        call(1);
    }

    /*
        Will this compile ??
        This is catching only one of the exception being thrown or
        more specifically Declared to be thrown.
    */

    void caller1() {
        try {
            call(0);
        }catch(java.io.FileNotFoundException e){

        }
    }
}
