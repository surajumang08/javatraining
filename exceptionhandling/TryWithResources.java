

/*
    TryWithResources synatax is
    try(declaration separated by semicolon){

    }
    optional catch and finally

    Some rules regarding declaration which can be present inside the resoueces
    section of the try block.

    declaration can only be of type Closeable and AutoCloseable.

    Resources which were acquired will automatically be released when the block
    finishes or an exception occurs so finally is made optional.

    If you want to declare variables of custom types then you must implemet
    either the Closeable or AutoCLosable interface.
*/

package exceptionhandling;

class Parent implements AutoCloseable{

    public String name;
    Parent (String name){
        this.name = name;
    }

    public void close() throws Exception {
        throw new Exception("From close" + name);
    }
}

public class TryWithResources {

    TryWithResources(){

    }

    public static void main(String[] args) {

        try{
            new TryWithResources().caller();
        }
        catch(Exception e) {
            System.out.println(e);

            for (Throwable t : e.getSuppressed()) {
                System.out.println(t);
            }
        }
    }

    public void caller() throws Exception{

        try(Parent p = new Parent("First");
            Parent q = new Parent("Second")){

                throw new Exception("From Try");

        }
        finally{
            System.out.println("finally");
        }
    }

}
