/*

*/
package exceptionhandling;

public class retFinal {
    /*
        If a method having a return type is throwing some checked Exception
        then writing the return statement in catch blocks won't work.
        The compiler will give error saying missing return statement.

        In case of Checked exceptionhandling
    */
    int show()
    {
        int a=10;
        try{
               int data=25/0;
               System.out.println(data);
               //return a;
               throw new IllegalStateException();
        }
        catch(ArithmeticException e){
             System.out.println(e);
             return a++;
        }
        catch(RuntimeException e){
            System.out.println("Parent");
            //return ++a;
        }
        finally{
            System.out.println("finally block is always executed");
            //return ++a;
        }



        // System.out.println("Rest of the body");
        //return a++;
        //return a;
    }

      public static void main(String args[]){
      retFinal r = new retFinal();
      System.out.println(r.show());
      }
}
