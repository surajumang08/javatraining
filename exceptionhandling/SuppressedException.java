/*
    When multiple exception are thrown during the execution of a try
    with resources block.

    A try with resources block releases resources in the opposite order
    by which it acquired them.

    So for each resource acquired in the try block they are released by
    calling the close() method on them.

    The close method can also throw some exception
*/
package exceptionhandling;

class Resource implements AutoCloseable {
    private String name;

    Resource(String name){
        this.name = name;
    }
    public void close() throws Exception {
        throw new java.io.IOException("From Close" + name);
    }
}

public class SuppressedException {

    public void call() throws Exception {

        /*
            Freeing of resources will happen in the reverse order of allocation.
            Resource r2's close() will be called first and then r1's.
            Since the close method throws an exception. Close for all the resources
            will be executed but only the exception thrown while closing the first
            resource will be thrown to the caller.

            If the following commented line is uncommented then the actual
            exception thrown to the caller will be that explicitly thrown from
            the try block and rest will be suppressed.

        */
        try(Resource r1 = new Resource("Resource One");
           Resource r2 = new Resource("Resource Two"); ){

            //throw new java.io.IOException("From Try");
        }
    }

    public static void main(String[] args) {
        try{
            new SuppressedException().call();
        }catch(Exception e){
            // prints  the caught exception which was thrown by r2.
            System.out.println(e);
            System.out.println("These were Suppressed");

            for (Throwable t : e.getSuppressed() ) {
                System.out.println(t + " " + t.getMessage());
            }
        }
    }
}
