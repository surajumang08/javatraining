/*
    Ques : If catch and finally both throws different Checked Exception
    then is it required to declare both of them?

    Ans : If CatchFinallyBothThrow then only the exception thrown by
    finally is required to be declared.

    If finally doesn't throw any exception then whatever exception the catch
    blocks are throwing(or their super class) must be declared.
*/
package exceptionhandling;

public class CatchFinallyBothThrow {

    public void call()  throws java.io.FileNotFoundException {

        try {
            throw new Exception();
        }catch(Exception e){
            // even though this is higher in hierarchy it is not required
            // to be declared in the throws clause.
            throw new java.io.IOException("from catch");
        }finally{
            throw new java.io.FileNotFoundException(" From finally");
        }
    }

    public static void main(String[] args) {
        try{
            new CatchFinallyBothThrow().call();
        }catch(Exception e){
            System.out.println(e.getMessage() + " " + e.getSuppressed().length);
        }
    }
}
