/*
    A checked exception which is a subclass of Exception but not of
    RuntimeeException must be either declared or handled.

    Exceptions are handled using the Try Catch block.
    A Try block contains the actual code which may throw some exception,
    while the catch block contains code which catches and decides what to
    do next.(Either to throw again to the caller or try to recover from it.)

    There can be multiple catch blocks followed by an optional finally block.
    The purpose of finally is to write clean up logic and free up the resources
    acquired before the exceptional condition was encountered.

    The try block must be followed by either a catch block or a finally block.
    [Either of them must follow the try block].

    The order of Exceptions being caught is important.
    multiple catch blocks must adhere to the Exception hierarchy. A parent
    exception must not be declared before a child exception to top down order.

    Only if some serious error occur then the finally may not execute otherwise
    the finally will always be executed irrespective of whether the exception
    occured or not handled by which catch block is irrelevant.

    It will be executed Even if the caught exception was rethrown by a catch
    block.

*/
package exceptionhandling;
// It will be considered a checked exception since it extends Exception.

public class SimpleExample extends Exception {
    public static void main(String[] args)  {
        System.out.println(args.length);

        try{
            for (String s : args ) {
                checkFood(s);
            }
        }
        catch(SimpleExample  | NullPointerException e){
            e.printStackTrace();
        }
    }

    public static void checkFood(String food) throws SimpleExample {
        if (food.contains("bad")) {
            throw new SimpleExample();
        }
        System.out.println("I like " + food);
    }
}
