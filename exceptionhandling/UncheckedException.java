/*
    Some common Unchecked exception.
    These are exception which the compiler ignores and they are thrwon by rhe
    jvm at runtime.

    There are Two ways in which an exception can be thrown.
    1   Programmatically (using throw )
    2   By the JVM. All RuntimeException and errors are thrown by the JVM.

    The reason why RuntimeException are not checked by the compiler is because
    the compiler can't ascertain whether the exception will be thrown or not.
    Whereas All the checked exception are thrown Programmatically so the compiler
    is aware of them and it enforces the handle or declare rule for CheckedException.

    1   IllegalArgumentException
    2   NumberFormatException   Integer.parseInt()
    3   ArithmeticException
        getCause() gives the actual cause.
*/
package exceptionhandling;

public class UncheckedException {
    private void call(){
        try{

        }catch(Exception e){

        }
    }
    public static void main(String[] args) {
        new UncheckedException().call();
    }
}
