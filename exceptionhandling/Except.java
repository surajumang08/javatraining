public class Except {

  int val;

  Except(int val){
    this.val = val;
  }

  void m1(){
    try{
        m2();
    }
    catch(Exception e){
      System.out.println("Caught in m1\n Continue Normally");
    }

  }

  void m2() {
    m3();
  }

  int m3(){
    return (2*val+1) / val;
  }

  public static void main(String[] args) {
    Except e1 = new Except(0);
    e1.m1();

  }
}
