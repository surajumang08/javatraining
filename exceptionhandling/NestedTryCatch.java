/*
    What is the flow of control in case of nested try catch.
*/
package exceptionhandling;

public class NestedTryCatch {
    public int first, second;

    public void call(int f, int s) {
        try{
            if( f == 1)
                throw new NullPointerException();   //thrown from outer try
                //it will simply be caught in the catch block of the outer try.
            try{
                //if(s == 1)
                    //throw new IndexOutOfBoundsException();
                    // This will first be matched inside this try's catch
                    // and if no suitable catch is found then it will be
                    // propogated to outer's catch blocks.
            }
            catch(Exception e){
                System.out.println("Inner catch");
            }
            finally{
                System.out.println("inner finally");
            }
        }
        catch(Exception e){
            System.out.println("Outer catch");
            throw e;
        }
        finally{
            System.out.println("Outer finally");
        }
    
    }

    public static void main(String[] args) {
        try{
            new NestedTryCatch().call(1 , 1);
        }catch(Exception e){
            System.out.println(e.getMessage() + " " + e.getSuppressed().length);
        }
    }
}
