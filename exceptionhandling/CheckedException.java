/*
    Some examples of checked exception.
    All Exceptions which are not subclass of RuntimeException are
    checked.

    eg. SqlException, IOException, FileNotFoundException, EOFException

    If a method has declared that it may throw a checked exception then the
    caller of that method must ensure either to handle it or to declare that it
    throws the same exception or it's parent otherwise a compile time errors
    occurs.

*/
package exceptionhandling;

public class CheckedException {
    public static void main(String[] args) throws java.io.IOException {
        new CheckedException().caller();
    }

    public void getName(String name) throws java.io.FileNotFoundException {
        throw new java.io.FileNotFoundException();
    }
    public void caller() throws java.io.IOException{
        getName("Hello");
    }
}
