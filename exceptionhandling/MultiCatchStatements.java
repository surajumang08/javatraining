package exceptionhandling;

/*
    Multicatch synatax
    try{

    }
    catch (SqlException | IOException e){

    }
        Note that the caught exception can only have a single name.
        Reassignment of the caught exception is discouraged and Illegal
        sometimes.
        It is legal to reassign the caught exception object whent there is
        only one class of exception present in the catch block
        eg catch(IOException e){
            e = new IOException();
        }

    The list of exception in the catch block must not have a parent child
    relationship otherwise a CTE occurs.
    eg catch(IOException | FileNotFoundException e)

    The order in whuch the exception are written doesn't matter.
*/
public class MultiCatchStatements {

}
