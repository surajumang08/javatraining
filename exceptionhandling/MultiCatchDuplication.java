/*
    This program demonstrates the exception being caught is not
    a subclass of exception rather one of the type declared by the method.
*/
package exceptionhandling;

public class MultiCatchDuplication {

    public void call() throws java.io.IOException, java.sql.SQLException {
        //throw new java.io.IOException("IO");
    }
    /*
        Check to see if this method decides to rethrow the caught Exception
        then what actions need to be performed.

        Can it throw any exception irrespective of what the called method
        throws or is there some correlation ??
    */
    public void caller()throws java.io.IOException, java.sql.SQLException{
        try {
            call();
            /*
            If you wish to throw a checked exception then it needs to be declared.
            throw new IllegalAccessException();
            */

            throw new NullPointerException();
            System.out.println("Hello");

        }catch(Exception e) {
            /* The caught exception can be assigned a new value but this is
            not encouraged as that caiugh exception object is lost.

            Assignment is not possible with multicatch block as the
            reference is made final.
            */
            System.out.println(e.getMessage() + " " + e.getClass().getName());
            // reassignment to any type is allowed here. why?
            //e = new java.io.FileNotFoundException();
            // doesn't catch all exception subclasses.
            // catches only those declared.

            throw e;
        }
    }

    public static void main(String[] args) {

        try{
            new MultiCatchDuplication().caller();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

}
