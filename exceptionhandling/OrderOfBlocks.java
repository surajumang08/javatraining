/*
    This code demonstrate the order in which the try catch finally blocks
    execute.
    The normal flow is like try then if any exception doesn't occurs then finally.
    1 try ---> finally
    If any exception happens in try then also finally and then catch.
    2   try ---> catch ---> finally

    If there is an exception in the try block and the catch and finally both
    throw some exception then which one will actually be thrown.
*/
package exceptionhandling;
public class OrderOfBlocks {

    public void call (int x){
        try{
            System.out.println("trying with " + x);
            System.out.println(452 / x);
        }
        catch(ArithmeticException e){
            System.out.println("Caught arithmetic exception" + e.getMessage());
            throw new IndexOutOfBoundsException("Thrown from catch");
        }
        finally{
            System.out.println("finally called");
        }
    }

    public static void main(String[] args) {
        OrderOfBlocks o = new OrderOfBlocks();
        o.call(1);
        try{
            o.call(0);
        }catch (Exception e) {
            System.out.println("caught in mian " + e.getMessage() + e.getSuppressed().length);
        }

        o.call(10);
    }
}
