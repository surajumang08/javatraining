public abstract class UsingThisInConstructor {
  int a;
  String name;

  UsingThisInConstructor(){
    a = 100;
    name = "Suraj";
  }
  UsingThisInConstructor(int a, String name) {
    this.a = a;
    this.name = name;
  }
  public abstract void show();
}

class Tester extends UsingThisInConstructor {
  int a = 15;

  public void show() {
    System.out.println(super.a + " " + name + a);
  }

  public Tester(){
    super(12, "Rahul");
  }

  public static void main(String[] args) {
    Tester t = new Tester();
    t.show();
  }
}
