/*
    Encapsulation is a means to impose data hiding.
    It is possible with the use of access modifiers

    Polymorphism is the ability of one message to be interpreted in more
    than one form.
    Like if the message doWork is passed to an employee then the employees
    will do the work depending on what they Actually are as employee is a
    generic term it can be reffering to a manager a designer a developer or
    an architect. So depending upon what they Actually are they will respond
    to the same message in a different manner.
*/
