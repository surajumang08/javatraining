public abstract class PrivateVariables {
  /*
    Variables and methods with definition can be private but abstract methods can't
  */
  private int num;
  private int noShow(){
    return 0;
  }
  /*
    abstract methods can't be declared private, all other modifiers can be used.
    The Following method will give CTE. Illegal combination abstract and private.
  */
  // private abstract methods();

  /*
    Similarly abstract can't be combined with static.
  */
  // protected abstract static void show();
  


}
