/*
  Checks if Final fields and methods are visible in the child classes or not.
*/
/* Intution : They must be visible in the Child class but the child class
              can't override the final method.

              Not sure whether the child class can hide the final fields or not.

*/

public abstract class VisibilityOfFinal {
  public static final String name = "ClassName";
  // It is an instance variable which is being initialized in the constructor.
  public final String lastName;

  public abstract void showName();

  VisibilityOfFinal(){
    lastName = "Suraj0";
  }
}

class TestVisibility extends VisibilityOfFinal {

  TestVisibility(String lastName){

  }

  public void showName(){
    System.out.println(lastName);
  }
  public static void main(String[] args) {
    TestVisibility v = new TestVisibility("HelloWorld");
    System.out.println(v.lastName);
  }
}
