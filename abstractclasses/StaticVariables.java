public class StaticVariables {
  private static int myVariable = 100;
  public static final String name;

  static{
    name = "Rahul Jangir";
  }

  private final String lastName ;

  StaticVariables(String lastName){
    this.lastName = lastName;
  }

}
