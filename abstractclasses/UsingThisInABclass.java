package abstractclasses;

abstract class Parent {
    int firstname;
    Parent(int firstname){
        // Where is `this` referring to ??
        
        this.firstname = firstname;
    }
    int getName(){
        return this.firstname;
    }
}

public class UsingThisInABclass extends Parent {
    int lastname;

    UsingThisInABclass(int lastname){
        super(452);
        // since we're able to use `this` here means the object is already
        // created, and it is simply being assigned.
        this.lastname = lastname ;
    }

    public static void main(String[] args) {
        UsingThisInABclass u = new UsingThisInABclass(120);

        System.out.println(u.getName() + " " + u.lastname);
    }
}
