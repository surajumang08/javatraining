
package scopeofvariables;

import classesandinterfaces.*;

public class ForLoop extends Apple {

    public static void main(String[] args) {
        ForLoop f = new ForLoop();

        f.method( new Apple() {
            public final static int x = 4520;
            public void method1(){
                System.out.println("Anonymous Apple" + x);
            }
        });
    }

    void method (Apple a) {
        a.method1();
        //a.meth();
    }
}
