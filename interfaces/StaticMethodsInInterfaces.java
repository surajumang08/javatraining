/*
    Interfaces should not provide static methods inside them
    as static methods can't be overridden.
    Or it can't provide an abstract static method.

*/

public interface StaticMethodsInInterfaces {
    static void callMe(){
        
    }

    void caller();
}
