class Me {
  public void showMe() {
    System.out.println("I am class Me");
  }
}

interface First {
  default void showMe() {
    System.out.println("I am First interface");
  }
}

interface Second {
  default void showMe() {
    System.out.println("I am Second interface");
  }
}

public class MultipleDefaultMethodsPrecedence extends Me implements First, Second {
  public static void main(String[] args) {
    MultipleDefaultMethodsPrecedence m = new
        MultipleDefaultMethodsPrecedence();

    m.showMe();
    
  }
}
