public interface OverloadingInterfaceMethods {
    public void twiddle(String s) ;
}

class Test implements OverloadingInterfaceMethods {
    public void twiddle(String s) {

    }
    public void twiddle(Integer s){

    }
}
