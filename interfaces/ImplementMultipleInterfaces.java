
package interfaces;

public class ImplementMultipleInterfaces {
    public static void main(String[] args) {
        // Upcastinf Interface to the upper class NOT allowed
        Parent p = new Concrete();

        // Concrete c = (Concrete)p;   //DownCast
        // c.eat();
        Apple a = (Apple)p;
        a.eat();
    }
}

class Parent {
    public void ask(){
        System.out.println("Asko");
    }
}

class Concrete extends Parent implements Apple, Banana, Mango {
    public void eat(){
        System.out.println("Hello");
    }
}

interface Apple {
    void eat();
}

interface Banana {
    void eat();
}

interface Mango {
    //void eat();
}
