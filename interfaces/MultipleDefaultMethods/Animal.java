public interface Animal {
  default void eat() {
    System.out.println("Animal Eating");
  }

  void cry();
}
