
package interfaces;

interface OverrideEquals {
     public int hashCode() ;
}

public class Equals implements OverrideEquals {

    public static void main(String[] args) {
        Equals e = new Equals();
        System.out.println(e.hashCode());
    }
}
