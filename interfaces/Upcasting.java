public class Upcasting {
    public static void main(String[] args) {
        Juicy a = new Apple();

        //cast to Pear;
        a.give();
        Pear p = (Pear)a;

        p.give();
    }
}

class Apple implements Juicy {
    public void give(){
        System.out.println("Apple is Juicy");
    }
}

class Pear implements Juicy {
    public void give(){
        System.out.println("Pear is Juicy");
    }
}

interface Juicy {
    void give();
}
