/*
  To count the frequency of each character present in a given String

  1: Report the occurences in the order of their appearence.
  2: Report the occurences in the natural ascending order.

*/

import java.util.Map;
import java.util.TreeMap;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Iterator;
class Main {

  String inputString;

  /*
    Prints the frequency of characters present in the String
    If order is true then input ordering is maintained otherwise ascending oreder is displayed.
  */
  void printFrequency(boolean order){
    Map<Character, Integer> counter = new TreeMap<Character, Integer>();

    for (int i = 0; i < inputString.length() ; i++ ) {
      try{
        int previousValue = counter.get(inputString.charAt(i));
        counter.put(inputString.charAt(i), previousValue + 1);
      }
      catch(Exception e){
        // It gets executed only for the first occurence of a character.
        counter.put(inputString.charAt(i), 1);
      }
    }

    /*

      Accessing the Map contents through an iterator
    */
    // Iterator it = counter.entrySet().iterator();
    // while(it.hasNext()){
    //   Map.Entry pair = (Map.Entry) it.next();
    //   System.out.println(pair.getKey() + ":" + pair.getValue());
    // }

    /*
      Another way to access Map contents
    */
    // for(char ch = 'a'; ch <= 'z'; ch++){
    //   try{
    //     int  c = counter.get(ch);
    //     System.out.println(ch + ":" + c );
    //   }
    //   catch(NullPointerException e){
    //
    //   }
    //
    // }

    Iterator it = counter.keySet().iterator();
    while(it.hasNext()){
      Character k = (Character)it.next();
      int c = counter.get(k);
      System.out.println(k + ":" + c);
    }
  }

  public static void main(String[] args) {

    Main m = new Main();
    m.inputString = args[0];
    m.printFrequency(true);
  }
}
