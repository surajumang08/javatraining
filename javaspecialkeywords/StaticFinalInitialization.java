public class StaticFinalInitialization {
    /*
        Do static final variable get the default value if they are not intialized
        at the time of declaration or in any static blocks.
    */

     static  int var = 452;
     final String s;

    //  static {
    //     var = 120;
    // }
    StaticFinalInitialization(){
        var = 452;
        s = "Dsishod";
    }
}
