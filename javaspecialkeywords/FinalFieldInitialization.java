/*
    Final fields can only be initialized once.
    1 : Either at the time of declaration.
    2 : Or inside every constructor.
    3 : It is a compile time error to not define a final Field.
    4 : Final methods and classes can be Overridden.
    5 : final can't be applied to abstract classes and interfaces.
    6 : ## final reference types an change their state.[not constant]
*/

public class FinalFieldInitialization {
  public final String name;
  private int var1;
  private int var2;

  FinalFieldInitialization(String name){
    this.name = name;
  }

  FinalFieldInitialization( int var1, int var2) {

    this.var1 = var1;
    this.var2 = var2;
    this.name = "Rahul";
  }

  public void show() {
    System.out.println(name + " " + var1 + " " + var2);
  }

  public static void main(String[] args) {
    FinalFieldInitialization f = new FinalFieldInitialization( 12, 13);
    f.show();
  }
}
