/*
    While final methods can't be overridden but they can surely be overloaded
    becaulse overloading a method doesn't interfere with the Parent's method.
*/

class Parent {
    final void callMe() {
        System.out.println("Parent's called");
    }
}

public class OverloadingFinalMethods extends Parent{
    void callMe(int a) {
        System.out.println("Child's called");
    }

    public static void main(String[] args) {
        OverloadingFinalMethods o = new OverloadingFinalMethods();
        o.callMe();    // Prent's version
        o.callMe(45); // child's version
    }
}
