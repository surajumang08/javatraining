/*
    final reference variable only means that once it is made to refer to
    a particular object then it can't refer to any other objects.
*/

public class ChangeStateOfFinalReference {
    final Person p;
    String Name = "Hello";
    /*
        Trying to Intialize twice will give a CTE.
    */
    ChangeStateOfFinalReference(){
        p = new Person("Saloni", 55);
    }

    ChangeStateOfFinalReference(String Name) {
        this.Name = Name;
        p = new Person("Saloni", 55);
    }

    void show(){
        System.out.println(Name + " " + p);
    }

    public static void main(String[] args) {
        //p = new Person("Suraj", 123);
        /*
            Creating an instance of this class which initializes the final
            variable p with "Saloni" but later it is changed to "Rahul"
        */
        ChangeStateOfFinalReference c = new ChangeStateOfFinalReference("Hello");
        c.p.setName("Rahul");
        c.show();
        
    }

}

class Person {
    String Name;
    int Age;

    Person(String name, int Age) {
        this.Name = name;
        this.Age = Age;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String toString(){
        return Name + " " + Age ;
    }
}
