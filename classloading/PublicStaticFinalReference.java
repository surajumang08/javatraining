class A {

    /*
        public static final must not be used because it bypasses the create
        method and makes the object directly accessible to outside code.

        final is neccessary if we are to make sure that the reference is
        not changed after initialization.

        static makes sure that there is only one copy of the reference and
        the same is passed whenever create is called.

    */
    private static final A a = new A(12); // what if new A() here !!

    private int val;
    private String name;

    private A(int val) {

    }
    private int A(int val){
        return 12;
    }

    static {
        //a = new A();
    }

    public static A create(int val, String name){
        a.val = val;
        a.name = name;
        return a;
    }

    public void show() {
        System.out.println(val + " " + name);
    }
}

public class PublicStaticFinalReference {
    /*
        This tests what happens if a public static final variable has a
        reference to the same class type
    */

    public static void main(String[] args) {
        A.create(12, "Suraj").show();
        A.create(1252, "Rahul").show();
    }
}
