
package classloading;

public class ArrayClass {

    ArrayClass(){
        System.out.println("Ctor called");
    }
    public static void main(String[] args) throws Exception {
        String st[] = new String[2];
        int a[][] = new int[4][3];
        int[] ac = {1,2,3,4};

        System.out.println(st.getClass().getName());
        System.out.println(ac + " " + ac.hashCode());
        System.out.println(a.hashCode() + " " + st.hashCode());

        int [][] aa = new int[4][];
        System.out.println(aa.equals(a));
        System.out.println(aa[3]);

    }
}
