class ClassLoadingAtRuntime {

  static int count = 4;

  static {
    System.out.println(count++);
  }

  public static void main(String[] args) throws ClassNotFoundException {

    Class prev = Class.forName("Rabbit");

    Class me = Class.forName("Rabbit", true, prev.getClassLoader() );
  }

}
