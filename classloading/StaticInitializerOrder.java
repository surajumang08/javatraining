public class StaticInitializerOrder {

  static {
    System.out.println("Hello");
    j = 45;
  }

  static int j = 10;
  int instance ;

  StaticInitializerOrder(int instance) {
      this.instance = instance;
  }

  public static void main(String[] args) {
    System.out.println(j);
  }
}
