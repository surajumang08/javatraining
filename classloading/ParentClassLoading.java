class AAA{
  static int k;

  static {
    System.out.println("loading AAAAAAAAAAA");
    k = 100;
  }
}

class A {
    static {
        System.out.println("loading A static 1");
    }
    static {
        System.out.println("loading A 2= " + AAA.k);
    }
    static {
        System.out.println("loading A static 3" + B.c);
    }
    static int a=10;
    A(){
    }
}

class B extends A{
    static {
       System.out.println("loading B A.a= " + A.a);
    }
    static int c = 50;
}

class Test {
    public static void main(String[] args) {
        new B();
    }
}
