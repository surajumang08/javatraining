/*
  The Apple class depends on this class.
  If you delete the Fruit.class file and try to run Apple using java
  then the JVM will report a NoCLassDefFoundError

  To get the ClassFormatError you can open the compiled class file using any\
  editor and remove some part of it and save and run java Apple.
  It will throw the ClassFormatError.
*/

public class Fruit {
  String Name = "FruitName";
}
