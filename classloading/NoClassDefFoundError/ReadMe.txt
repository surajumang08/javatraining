1 :class Apple extends class Fruit. Compile both the java files
   to generate two class files. Apple.class and Fruit.class

2 : Open the Fruit.class file in any text editor and remove some of the
    text from it and run `java Apple` It will give the ClassFormatError.

3 : Now delete the Fruit.class and run `java Apple`
    It will give you the NoClassDefFoundError. 
