package annotat;
import java.lang.reflect.*;
import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface NameCheck{
    String pattern() default "";
}
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface DateCheck{
    String pattern() default "";
}
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface NumberCheck{
    String pattern() default "";
    boolean required() default true;

}

class Anno {

    @NameCheck(pattern="sdfs")
    public Boolean call(){
        return new Boolean(true);
    }

    private int call2(){
        return 12;
    }


}

public class MyAnno {
    public static void main(String[] args) {
        Anno a = new Anno();
        System.out.println("HelloWorld");

        for (Method m : a.getClass().getMethods()) {
            System.out.println(m);
            System.out.println(m.getGenericReturnType().class);
            System.out.println(m.getReturnType().class);
        }
    }
}
