package collections.maps;
import java.util.*;
import classesandinterfaces.*;
import static collections.FruitCollection.*;

public class linkedHashMap {

    public static void main(String[] args) {
        LinkedHashMap<String, Fruit> lm = new LinkedHashMap<>(fruitMap);

        lm.put(null, new Fruit("Hello world"));
        lm.put(null, new Fruit("MEorld"));

        lm.forEach( (k, v) -> System.out.println(k + "--->" + v) );
    }
}
