/*
    difference between HashMap and LinkedHashMap

*/

package collections.maps;
import java.util.*;
import classesandinterfaces.*;
import static collections.FruitCollection.*;

public class hashMap {


    public static void main(String[] args) {
        hashMap hh = new hashMap();

        // order by input.
        print(fruitMap);

        Map<String, Fruit> mm = new HashMap<>(fruitMap);
        mm.put(null, new Apple("NUll apple"));
        mm.put(null, new Apple("Another Null"));
        mm.put("Hello", null);
        print(mm);

    }
}
