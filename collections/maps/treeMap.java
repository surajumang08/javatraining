/*
    A TreeMap sorts by key or by value.

    If a key is used which is not comparable (ie doesn't implement the comparable)
    interface. Then a runtime error class can't be cast to comaparable will be thrown


*/
package collections.maps;

import java.util.*;
import classesandinterfaces.*;
import static collections.FruitCollection.*;

class Temp {

}

public class treeMap {
        treeMap(){
                return;
        }
    public static void main(String[] args) {
        System.out.println("Normal ordering");
        print(fruitMap);
        System.out.println("TreeMap ordering");

        // Map<String, Fruit> m = new TreeMap<String, Fruit>((a, b) -> b.compareTo(a));
        //
        // m.putAll(fruitMap);
        // print(m);

        TreeMap<Temp, String> tm = new TreeMap<>();
        tm.put(new Temp(), "Suraj");
    }
}
