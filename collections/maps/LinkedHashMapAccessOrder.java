/*
    These are Ordered collections so they have a predictable access order.
    1   Insertion Order.
    2   LastAccess Order [set using a boolean flag]

    The order is from least recently accessed to Most recently used

    What happens to s LinkedHashMap when an entry is put inside the map
    which is already there (same key and  value ). No Insertion occurs in
    this case and hence it will not be counted as an access.

    1   put, putIfAbsent, get, getOrDefault, compute, computeIfAbsent,
        computeIfPresent, merge.

        successful invocation of only these methods will be counted as an access.
*/

package collections.maps;

import static collections.FruitCollection.*;
import classesandinterfaces.*;
import java.util.*;

public class LinkedHashMapAccessOrder {

    public void insertionOrder(LinkedHashMap<String, Fruit> fruits) {
        String keys[] = {"JuicyMango", "AmericalGoldenDel", "GoldenApple"};
        for(String i : keys){
            System.out.println(fruits.get(i));
        }
    }

    public void accessNotCounted (LinkedHashMap<String, Fruit> fruits) {

        String keys[] = {"JuicyMango", "AmericalGoldenDel", "GoldenApple"};
        for(String i : keys){
            // putting in the exact same entry.
            System.out.println(fruits.put(i, new Fruit(i)));
        }
    }

    public void accessOrder(LinkedHashMap<String, Fruit> fruits) {
        print(fruits);
    }

    public static void main(String[] args) {
        print(fruitMap);

        LinkedHashMapAccessOrder lh = new LinkedHashMapAccessOrder();
        LinkedHashMap<String, Fruit> lm = new LinkedHashMap<>(16, 0.7f, true);

        //fruitMap.forEach( (s, f) -> lm.put(s, f) );

        lm.putAll(fruitMap);

        //lh.insertionOrder(lm);
        lh.accessNotCounted(lm);
        System.out.println();
        lh.accessOrder(lm);
    }

}
