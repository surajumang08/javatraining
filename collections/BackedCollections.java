/*
    The TreeMap and TreeSet collection return a backed collection when
    some of their methods are invoked like.
    1   subSet, subMap
    2   headSet, headMap
    3   tailSet, tailMap

    Modifiying the backed collection will also modify the parent collection.
    And Modifiying the parent may change the Backed collection as well if the
    added or removed element is a part of it.
*/

package collections;

import static collections.FruitCollection.*;
import classesandinterfaces.*;
import java.util.*;

public class BackedCollections {

    public static void main(String[] args) {
        // trying with headSet.

        TreeSet<Fruit> fruits = new TreeSet<>(fruitMap.values());
        /*
            Returns a Set from the beginning to the ocuurence of the passed
            Argument.
            Try chnging the returned collection and see if it changes the
            original or not.

        */
        print(fruits);
        TreeSet<Fruit> headFruit = (TreeSet)fruits.headSet(new Fruit("GoldenApple"), true);

        print(headFruit);
        // from A to G
        fruits.remove(new Fruit("Banana"));
        print(headFruit);
    }

    public static void add() {

    }

    public static void remove() {

    }
}
