
package collections.sets;
import java.util.*;
import classesandinterfaces.*;

public class LegacyTreeSet {
    public static void main(String[] args) {
        Set<Fruit> s = new TreeSet<>();
        s.add(new Fruit("Apple"));
        s.add(new Apple("Coala"));
        s.add(new Mango("Banana"));


        Iterator i = s.iterator();
        while(i.hasNext()){
            Fruit f = (Fruit) i.next();
            System.out.println(f.getName() + " " + f);
        }
    }
}
