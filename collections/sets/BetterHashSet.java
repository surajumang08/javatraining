package collections.sets;
import classesandinterfaces.*;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
/*
    HashSet is a collection which contains non duplicate objects.
    It is required that the hashcode method alongwith the equals method be
    overridden properly.

    This example shows what happens when the above requirements are not
    satisfied.
    class BetterApple overrides only equals not hashcode.

    every Apple instance is not unique now but the contract is not maintained.
    The object's version will be used.

    HashSet provides
        add()       boolean
        remove()    boolean
        isEmpty()   boolean
        contains()  boolean
        size()      int
        clear()     void
        iterator()  Iterator<E>

*/
public class BetterHashSet {
    // Creates a Hashed Set with default size.
    static class BetterApple extends Apple {

        public BetterApple(String name){
            super(name);
        }
        public boolean equals(Object o){
            //using the equals method of String.

            return  (o instanceof Apple) ? ((Apple)o).getName().equals(this.getName()) : false ;
        }
        /*
            If equals is overridden but not hashCode then the contract
            will be void.
            The contract says that if x.equals(y) is true then
                x.hashcode() must be equal to y.hashcode()
            but the default hashcode() gives a unique value for each
            object based on the exact memory area they are stored in.

        */

        public int hashCode(){
            /*
                Delegate the call to String's hashcode().
            */
            return this.getName().hashCode();
        }
    }

    static Set<BetterApple> apples = new HashSet<>();

    public static void main(String[] args) {

        /*
            Now both of these BetterApples are equal
            according to equals.
            But still
        */
        BetterApple a, b, c ;
        apples.add(a = new BetterApple("Juicy"));
        apples.add(b = new BetterApple("Juicy"));

        System.out.println(a.equals(b));

        apples.add(c = new BetterApple("Sour"));

        /*
            Different ways of Iterating through the collection.
        */
        Iterator<BetterApple> it = apples.iterator();
        while(it.hasNext()){
            System.out.println( it.next().getName() );
        }
        /*
            Still three Apples
            with hashcode implemented the size will be only 2.
        */
        System.out.println(apples.size());
    }



}
