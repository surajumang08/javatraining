/*

*/
package collections.sets;
import collections.FruitCollection;
import java.util.*;
import classesandinterfaces.*;

class Dog  implements Comparable<Dog> {
        public String name;
        Dog(){

        }
        Dog(String name){
                this.name = name;
        }
        public String getName(){
                return name;
        }
        public int compareTo(Dog d) {
                return this.name.compareTo(d.name);
        }
        // public boolean equals(Object o){
        //         return (o instanceof Dog) ? (this.getName().equals(((Dog)o).getName())) : false;
        // }
        public int hashCode(){
                return this.getName().hashCode();
        }
        public String toString(){
                return name;
        }
}

class hashSetWithComparable {
        public static void main(String[] args) {
                Dog[] d = new Dog[]{
                        new Dog("Harry"), new Dog("Tommy"), new Dog("Harry"),
                };

                Set<Dog> dogs = new HashSet<>(Arrays.asList(d));
                dogs.add(new Dog("Harry"));
                dogs.add(new Dog("Tommy"));
                dogs.add(new Dog("Harry"));

                FruitCollection.print(dogs);

        }
}
