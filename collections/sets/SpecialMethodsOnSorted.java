/*
    Some special methods that are applicable on the ordered collections
    TreeSet and TreeMap.
    1   lower()
    2   floor()
    3   ceiling()
    4   higher()

    they also provide with Backed collections which are nothing but a
    copy (subset) of a collections such that they are reffering to the same
    collection.

    1   headSet( e)
    2   tailSet(e)
    3   subSet(s, e)

    there are similar methods for Maps as well.
    These methods are available only to TreeSet and TreeMap.

    The meaning of these functions should be self explanatory.

*/

package collections.sets;
import collections.FruitCollection;
import java.util.*;

public class SpecialMethodsOnSorted {
    public static void main(String[] args) {
        /*
            We already have a Map from Integer to Integer and a set of Integer.
        */
        System.out.println(FruitCollection.intMap);
        System.out.println(FruitCollection.intSet);
        System.out.println(FruitCollection.intSet instanceof TreeSet);

        TreeSet<Integer> t = (TreeSet)FruitCollection.intSet;
        System.out.println(t.subSet(24, 75));
        System.out.println(t.headSet(45));
        System.out.println(t.tailSet(45));

        
    }
}
