package collections.sets;
import classesandinterfaces.*;
import static collections.FruitCollection.*;
import java.util.*;
/*
    HashSet is a collection which contains non duplicate objects.
    It is required that the hashcode method alongwith the equals method be
    overridden properly.

    This example shows what happens when the above requirements are not
    satisfied.
    class Apple overrides neither equals nor hashcode.

    every Apple instance is unique if the equals method is not overridden.
    The object's version will be used.

    HashSet provides
        add()       boolean
        remove(Object)    boolean
        isEmpty()   boolean
        contains(Object)  boolean
        size()      int
        clear()     void
        iterator()  Iterator<E>
*/
public class hashSet {
    // Creates a Hashed Set with default size.
    static Set<Apple> apples = new HashSet<>();

    public static void main(String[] args) {

        apples.add(new Apple("Juicy"));
        apples.add(new Apple("Juicy"));

        apples.add(new Apple("Sour"));
        apples.add(null);

        //print(apples);

        /*
            Different ways of Iterating through the collection.
        */
        Iterator<Apple> it = apples.iterator();
        while(it.hasNext()){
            System.out.println( it.next());
        }
        System.out.println(apples.size());
    }



}
