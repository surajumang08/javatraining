/*
    Implements only the Queue interface which is a

    Contrary to how a queue behaves this collection sorts the elements
    based on some criteria and then provides access to them such that
    they appear from the highest priority to lowest priority.

    Sorted queue of the most restricted type. Access is allowed only from the
    head. An oredering can be defined at the time of creation otherwise the
    natural ordering will be used.

    [What happens when the elements being added have no natural ordering ?]

    Some methods
    1   add, offer
    2   peek,
    3   poll, remove
    4   contains,
    5   iterator, comparator, spiltiterator

    Iterators of the priority queue are not guranteed to traverse in any particular
    order


*/
package collections.queue;

import static collections.FruitCollection.*;
import java.util.*;
import classesandinterfaces.*;

public class priorityQueue {
    public static void main(String[] args) {

        // natural ordering
        PriorityQueue <Integer> t = new PriorityQueue<>(intSet);

        // while(t.size() != 0){
        //     System.out.println(t.poll());
        // }

        Iterator <Integer> i = t.iterator();
        // i.next();
        // i.next();
        // i.remove(); // removes 4
        // i.forEachRemaining( System.out::println );
        int k = 0;
        while(i.hasNext()){
            System.out.println(i.next());
            if(k++ % 2 == 1){
                i.remove();
            }

        }

        System.out.println();
        t.iterator().forEachRemaining( System.out::println);

        // AbstractQueue<Fruit> p = new PriorityQueue<>(Collections.reverseOrder());
        // p.addAll(fruitMap.values());
        //
        // while (p.size() != 0) {
        //     System.out.println(p.poll());
        // }
    }
}
