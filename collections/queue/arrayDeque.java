/*
    ArrayDeque are implemetation of deque interface which uses an array of
    Objects. `Object[] table`.
    But unlike linked list it does not provide implementation for the
    List interface so random access is not allowed.

    The only methods available are those which traverse the list either
    from back or from front.

    1   addFirst, addLast, offerFirst, offerLast
    2   removeFirst, removeLast, pollFirst, pollLast
    3   getFirst, getLast, peekFirst, peekLast
    4   add, offer, remove, poll, peek, push, pop, delete, element,
    5   size, isEmpty,
    6   remove(), retainAll, removeAll, contains, forEach, removeIf
    7   iterator, splititerator, descendingIterator
*/
