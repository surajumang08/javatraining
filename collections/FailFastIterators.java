/*
    If an Iterator is created for a collection and after that the
    collection is structurally modified in any way other than using the
    iterator's method (remove, add) then the Iterator will fail throwing
    some exception.

    They are called failFast because they will throw the exception immediately
    without waiting but it is not guranteed.

*/

package collections;

import static collections.FruitCollection.*;
import classesandinterfaces.*;
import java.util.*;

public class FailFastIterators {
    public static void main(String[] args) {
        LinkedHashSet< Fruit> lhs = new LinkedHashSet<>(fruitMap.values());

        Iterator<Fruit> i = lhs.iterator();

        // If the collection is modified at this point then the iterator will
        // fail.
        i.forEachRemaining( System.out :: println);

        lhs.add(new Fruit("HelloWorld"));

        lhs.iterator().forEachRemaining( System.out :: println);
    }
}
