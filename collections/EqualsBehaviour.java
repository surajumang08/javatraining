package collections;

public class EqualsBehaviour {
    String name;
    public static void main(String[] args) {
        /*
            Name is set to null so trying to access Equals on it will
            throw NullPointerException.
            System.out.println(new EqualsBehaviour().name.equals("Suraj"));
        */

        /*
            x.equals(null) must always return False.
            Normally we do an instanceof test inside the equals method
            while overriding it.

            So if
            public boolean equals(Object o) {
                if(o instanceof ClassName)
            }
            is null instanceof Object ??
            null instanceof AnyClass must always be False.
        */
        System.out.println(null instanceof Object);

    }
}
