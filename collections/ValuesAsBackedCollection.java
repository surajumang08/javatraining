/*
    Backed collection obtained through values methods of Map or[any other collection]
    is refferring to the parent collection on which it was created.

    Any deletion using the backed collection will be reflected in the parent
    as well.

    Add or addall operation is not allowed on this backed collection.
*/
package collections;

import static collections.FruitCollection.*;
import classesandinterfaces.*;
import java.util.*;

public class ValuesAsBackedCollection {

    public static void main(String[] args) {
        LinkedHashMap<String, Fruit> fruits = new LinkedHashMap<>(fruitMap);
        // this is a backed collection.
        Collection<Fruit> backedFruits = fruits.values();
        print(backedFruits);
        System.out.println(backedFruits.getClass().getName());

        /* Addition to a backed collection in case of a Map is not allowed.
            addtoBacked(backedFruits);
        */
        removefromBacked(backedFruits);
        print(fruits);
        print(backedFruits);
    }

    public static void addtoBacked(Collection<Fruit> backedFruits) {
        // adding to a backed collection is not allowed.
        backedFruits.add(new Apple("Hello"));
    }

    public static void removefromBacked(Collection<Fruit> backedFruits) {
        System.out.println(backedFruits.remove(new GoldenDel("JuicyMango")));
        //System.out.println(backedFruits);
    }
}
