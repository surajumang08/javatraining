
package collections;

import java.util.*;
import classesandinterfaces.*;

public class FruitCollection {
    public static final Map<String, Fruit> fruitMap;
    public static final List<Fruit> fruitList;
    public static final Set<Fruit> fruitSet;
    public static final Set<Integer> intSet;
    public static final Map<Integer, Integer> intMap;

    static {
        fruitMap = new LinkedHashMap<>();

        // add some items to the Map
        String s;
        fruitMap.put(s = "GoldenApple", new Apple(s));
        fruitMap.put("GoldenApp", new Apple(s));
        fruitMap.put("GoldenAp", new GoldenDel(s));

        fruitMap.put(s = "JuicyMango", new Mango(s));
        fruitMap.put(s = "TastyAlphonse", new Alphonse(s));
        fruitMap.put("TastyAlphon", new Mango(s));

        fruitMap.put(s = "AmericalGoldenDel", new GoldenDel(s));
        fruitMap.put(s = "General Fruit", new Fruit(s));


        // put the same inside the List.
        fruitList = new LinkedList<>(fruitMap.values());
        fruitList.add(new Apple("GoldenApple"));
        fruitList.add(new GoldenDel("GoldenApple"));
        fruitList.add(new Mango("TastyAlphonse"));

        fruitSet = new HashSet<>(fruitList);

        /*
            Initialize the intSet and intMap.
        */
        intMap = new HashMap<>();
        for (int i =10; i>0 ; i-- ) {
            intMap.put(i, i*i);
        }

        intSet = new TreeSet<>(intMap.values());

    }

    public static <K, V>void print(Map<?extends K, ? extends V> name){

        name.forEach(new java.util.function.BiConsumer<K, V>(){
            public void accept(K s, V f){
                System.out.println(s + "---> " + f);
            }
        });
        System.out.println("\n");
    }

    public static <T> void  print(Collection<? extends T> name) {
        System.out.println("called second");
        for (T el : name) {
            System.out.println(el);
        }
        System.out.println("\n");
    }
}
