/*
    Supports parallel programming which Iterator and ListIterator don't.
    
    Can be used with both collection and streams.

    They can't be used for Map implemented class.

    SplitIterators provide methods which can be used to provide multiprogramming.

    1   Splitting the source data
    2   Proocessing the data

    methods
    1   tryAdvance
    2   forEachRemaining
    3   trySplit

*/
