/*

*/
package collections.lists;
import classesandinterfaces.*;

public class GenericClass <T extends Fruit> {
    T t;
    GenericClass(T t) {
        this.t = t;
    }

    // public String toString(){
    //     return t.toString();
    // }

    public void show(){
        t.eat();
    }

    public static void main(String[] args) {
        System.out.println(new GenericClass <Apple> (new Apple("Hello")));
        System.out.println(new GenericClass(new Fruit("GHha")));
    }
}
