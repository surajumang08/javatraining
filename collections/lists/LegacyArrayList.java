
package collections.lists;
import java.util.*;
import classesandinterfaces.*;

public class LegacyArrayList {
    public static void main(String[] args) {

        //new LegacyArrayList().generic();
        new LegacyArrayList().legacy();
    }

    void generic(){
        List<Fruit> s = new ArrayList<>();
        s.add(new Fruit("Apple"));
        s.add(new Apple("Coala"));
        s.add(new Mango("Banana"));

        Iterator i = s.iterator();
        while(i.hasNext()){
            System.out.println(i.next() + " ");
        }
    }

    void legacy(){
        List s = new ArrayList();
        s.add(new Fruit("Apple"));
        s.add(new Apple("Coala"));
        s.add(new Mango("Banana"));
        s.add("HeloWorld");

        Iterator i = s.iterator();
        while(i.hasNext()){
            System.out.println(i.next() + " ");
        }
    }
}
