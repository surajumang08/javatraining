/*
    Since LinkedList implement both List and Queue they can be accessed
    randomly or in a restricted LIFO manner(using the methods of the Queue)
    [offer, poll, peek, ] { I am yet to try it though}.

    LinkedList
*/
package collections.lists;
import static collections.FruitCollection.*;
import classesandinterfaces.*;
import java.util.*;

public class LinkedListAccessOrder {
    // create a linked list and try accesing it using the Queue methods.
    public static void main(String[] args) {
        print(fruitMap);
        LinkedList<Fruit> fruits = new LinkedList<>(fruitMap.values());
        print(fruits);

        LinkedListAccessOrder ll = new LinkedListAccessOrder();

        ll.listMethods(fruits);
        ll.queueMethods(fruits);
    }

    public void queueMethods(LinkedList<Fruit> fruits){
        fruits.offer(new Apple("My Apple"));
        fruits.add(new Apple("Another apple"));
        //fruits.

        while(0 != fruits.size()){
            System.out.println(fruits.pollLast());
        }
    }

    public void listMethods(LinkedList<Fruit> fruits) {
        for (int i = fruits.size()-1; i>=0 ; i--) {
            System.out.println(fruits.get(i));
        }
    }
}
