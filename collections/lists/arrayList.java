package collections.lists;

import java.util.*;
import classesandinterfaces.*;
import static collections.FruitCollection.*;

public class arrayList {
    public static void main(String[] args) {
        /*
            creating an arrayList from a LinkedHashMap. It will
            give the elements in the insertion order.
        */
        ArrayList<Fruit> flist = new ArrayList<>(fruitMap.values());
        print(flist);

        flist = new ArrayList<>(new HashMap<>(fruitMap).values());
        print(flist);
    }
}
