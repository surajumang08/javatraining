
package multithreading;

public class Synchronized {
  public static void main(String[] args) {
    Account account = new Account(100);
    Thread t1 = new Thread(new Transaction(account));
    Thread t2 = new Thread(new Transaction(account));
    Thread t3 = new Thread(new Transaction(account));

    t1.start();
    t2.start();
    t3.start();
  }
}

class Transaction implements Runnable{
    Account account;

    Transaction(Account account){
      this.account = account;
    }

    public void run(){
        synchronized(account){
          System.out.println(account.getBalance() + " " + Thread.currentThread().getName());
          try {
            Thread.sleep(7000);
          } catch(Exception e) {

          }
          account.credit(400);
          System.out.println(account.getBalance() + " " + Thread.currentThread().getName());
        }
    }
}

class Account{
  private int balance;

  Account(int balance){
    this.balance = balance;
  }

  public int getBalance(){
    System.out.print("Printing balance");
    return balance;
  }

  public synchronized void credit(int val){
    balance += val;
  }

  public synchronized void debit(int val){
    balance -= val;
  }
}
