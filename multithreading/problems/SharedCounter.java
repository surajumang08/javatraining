/*
        A program to increment a shared counter using multole threads in parallel

        It gives inconsistent values even with the volatile modifier.

        Try to use yield and sleep to make it even more inconsistent.

*/
package multithreading.problems;

class Value implements Runnable {
        public long val = 0;

        public void run(){
                for(int i = 0; i<10; i++){
                        val++;
                        System.out.println(val);
                }

        }
}

public class SharedCounter {
        public static void main(String[] args) {

                Thread[] t = new Thread[10];
                Value v = new Value();

                for(int i = 0; i<10; i++){
                        t[i] = new Thread(v);
                }

                for(int i = 0; i<10; i++){
                        t[i].start();
                }

                try{
                        Thread.currentThread().sleep(2000);
                }catch(Exception e){

                }

                System.out.println(v.val);
        }
}
