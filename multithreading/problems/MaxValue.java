/*
        https://www.cs.carleton.edu/faculty/dmusican/cs348/java_multi/
        
        Crete four threads to find the maximum from an Array.
        Assume the Array has atleast four elements.
*/
package multithreading.problems;

class ArrayVal implements Runnable{

        public int[] val;
        private int left, right;
        private static int thresh;

        ArrayVal(){
                this(20);
        }
        ArrayVal(int num){
                val = new int[num];
                thresh += val.length/4;
                init();
        }

        private void init(){
                fillRandom();
        }

        public void run(){
                int m1=0, m2=0, m3=0, m4=0;
                if(Thread.currentThread().getName().equals("First")){
                        m1 = max(0, thresh);
                }
                else if (Thread.currentThread().getName().equals("Second")){
                        m2 = max(thresh, 2*thresh);
                }
                else if(Thread.currentThread().getName().equals("Third")){
                        m3 = max(2*thresh, 3*thresh);
                }
                else{
                        m4 = max(3*thresh, val.length);
                }
                System.out.println(m1 + " " + m2 + " "+ m3 + " " + m4);
        }

        public void fillRandom() {
                for (int i = 0; i < val.length ; i++ ) {
                        val[i] = (int)(Math.random() * 100.0);
                }
        }
        public int max(int l, int r){
                int v = 0;
                for (int i = l; i < r; i++ ){
                        if(val[i] > v)
                                v = val[i];
                }
                return v;
        }
}

public class MaxValue {

        public static void main(String[] args) {
                ArrayVal arr = new ArrayVal(Integer.parseInt(args[0]));

                for (int i = 1; i < arr.val.length; i++ )
                        System.out.print(arr.val[i] + " ");
                System.out.println();

                Thread t1 = new Thread(arr, "First");
                Thread t2 = new Thread(arr, "Second");
                Thread t3 = new Thread(arr, "Third");
                Thread t4 = new Thread(arr, "Fourth");

                t1.start();
                t2.start();
                t3.start();
                t4.start();
        }
}
