public class NoSynchronization {
  static int i= 0, j = 0;

  public void one(){
      i++; j++;
  }

  public void two() {
    System.out.println(i + " " + j);
  }
}

class Runner extends NoSynchronization implements Runnable {
  boolean run1;

  Runner(boolean run1){
    this.run1 = run1;
  }

  public void run(){
    for (int i =0; i < 100; i++ ) {
      if(run1 == true){
          run1 = false;
          one();
      }
      else{
          run1 = true;
          two();
      }
    }

  }
}

class Test {
  public static void main(String[] args) {
    Runner r1 = new Runner(true);

    Thread t1 = new Thread(r1);
    Thread t2 = new Thread(r1);
    t1.setName("Thread 1");
    t2.setName("Thread 2");

    t1.start();
    t2.start();
  }
}
