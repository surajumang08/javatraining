/*

*/
package multithreading;

public class AnotherThread implements Runnable {

    private String name;
    private int value;

    AnotherThread(){

    }

    AnotherThread(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void run() {
        show();
    }

    public void loop() {
        for(int i = 0; i < 10; i++)
            System.out.println(i);
    }

    public void show() {
        throw new NullPointerException();
    }
}
