/*
        Synchronised methods both static and non-static can be entered by a
        thread only after acquiring a lock on an object i.e Class object for
        static and actual instance object for non-static.
        
*/
