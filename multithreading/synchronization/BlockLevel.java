/*
        Block level synchronisation lock

        When only a block of code needs to be synchronized then making the
        whole method synchronized will impact the performance as invoking a
        synchronized method has an overhead which can be 8 to 10 times that
        of a normal method.

         Further a block level lock can also be acquired for a different object
         than 
*/

package multithreading;

public class BlockLevel {



        public void access() {

        }

}
