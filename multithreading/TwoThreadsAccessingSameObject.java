

package multithreading;

class Local implements Runnable {
        private  int value;

        public void run(){
                if(Thread.currentThread().getName().equals("Increment"))
                        increment();
                else
                        decrement();
        }

        public void increment(){
                for(int i = 0; i < 20; i++){
                        System.out.println(++value + " " + --value + Thread.currentThread().getName());
                }
        }

        public void decrement(){
                for(int i = 0; i < 20; i++){
                        System.out.println(--value + " " + ++value);
                }

        }
}

public class TwoThreadsAccessingSameObject {

        public static void main(String[] args) {
                Local l1 = new Local();
                Thread inc = new Thread(l1, "Increment");
                Thread dec = new Thread(l1, "Decrement");

                inc.start();
                inc.start();
                dec.start();
        }
}
