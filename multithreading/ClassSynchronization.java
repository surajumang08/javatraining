/*
        Class synchronisation is required when a lock is required on class
        level fields(static) or acquiring a lock on a static method.

        Lock can simple be acquired using [ClassName.class]. It is also called
        a class literal which is refferring to the instance of java.lang.Class
        corresponding to ClassName.

        Another way to acquire the lock is by using Class.forName("ClassName").

*/
package multithreading;
