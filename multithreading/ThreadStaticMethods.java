/*
    There are some methods in the java.lang.Thread package which are static
    How are they different from method which are not static.

    1   currentThread, yield, sleep, onSpinWait
    2   enumerate, activeCOunt, dumpStack,
    3   holdsLock
    4   getAllStackTraces
    5   setDefaultUncaughtHandler, getDefaultUncaughtExceptionHandler

    sleep puts the currently executing Thread to sleep. As it throws a
    InterruotedExcption which is a checked exception. The call must be inside
    a try catch or the calling method must declare to throw it.

    
*/

public class ThreadStaticMethods {

}
