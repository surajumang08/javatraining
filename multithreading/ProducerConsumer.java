
package multithreading;

class Buffer{
        public int size = 5;
        public static final int limit = 10;
}

class Producer implements Runnable {
        Buffer b;
        Producer(Buffer b){
                this.b = b;
        }

        public void produce(){
                b.size++;
        }
        public void run(){
                while(true){
                        synchronized(b){
                                if(b.size == Buffer.limit){
                                        try{
                                                b.wait();
                                        }catch(InterruptedException e){

                                        }
                                }
                                while(b.size < Buffer.limit){
                                        System.out.println("Producing " + b.size);
                                        produce();
                                        b.notify();
                                }
                        }
                }


        }

}

class Consumer implements Runnable {
        Buffer b;
        Consumer(Buffer b){
                this.b = b;
        }
        public void consume(){
                b.size--;
        }
        public void run(){
                while(true){
                        synchronized(b){
                                if(b.size == 0){
                                        try{
                                                b.wait();
                                        }catch(InterruptedException e){

                                        }
                                }
                                while(b.size > 0){
                                        System.out.println("Consuming " + b.size);
                                        consume();
                                        b.notify();
                                }
                        }
                }

        }
}

public class ProducerConsumer {
        public static void main(String[] args) {
                Buffer b = new Buffer();

                Thread t1 = new Thread(new Producer(b));
                Thread t2 = new Thread(new Consumer(b));

                t2.start();
                t1.start();

        }
}
