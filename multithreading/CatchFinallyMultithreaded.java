/*
    This example shows that the catch anf finally blocks are executed
    by different threads.


*/
package multithreading;

public class CatchFinallyMultithreaded {

    public static void main(String[] args) {
        try{
            System.out.println("Hello Worls  1");
            throw new NullPointerException();
        }
        catch(NullPointerException e){

            System.out.println("Hello Worls  2");
            // for (int i = 0; i<10 ;i++ ) {
            //     // System.out.println("Catch");
            // }
            e.printStackTrace();

            System.out.println("Hello Worls  3");
        }
        finally{
            System.out.println("finally");
        }
    }
}
