/*
    his demonstrantes the behavior and differences between these methods.
    wait can only be called inside a synchronized block.

    Can it be called from a static synchronized block??
*/
package multithreading;

class Helper implements Runnable {

        private boolean check = false;

        public void set(boolean b){
                check = b;
        }

        public void run(){
                caller();
                call();
        }

        public synchronized void call() {
                try{
                        notify();
                        System.out.println("HIIIii");
                }
                catch(Exception e){

                }
        }

        public synchronized  void caller() {
                try{
                        if(check == false)
                                wait();
                        System.out.println("Hello from caller");
                }catch(Exception e){

                }
        }
}

public class WaitNotifyAndSleep {
        public static void main(String[] args) {
                Helper h = new Helper();

                Thread t1 = new Thread(h);
                Thread t2 = new Thread(h);
                t1.start();
                h.set(true);
                t2.start();

        }
}
