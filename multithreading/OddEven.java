/*
        WHat happens when a notify is called when no thread are waiting.

*/
package multithreading;

class Printer {
        static int currentVal = 0;
        boolean flag;

        Printer(){
        }
        // public void even(){
        //
        //         if(currentVal % 2 == 1){
        //                 try {
        //                         wait()
        //                 } catch(Exception e) {
        //
        //                 }
        //         }
        //         System.out.println(Thread.currentThread().getName() + " " +currentVal++);
        //
        //
        // }

        // public void odd(){
        //
        //         if(currentVal % 2 != 1){
        //                 try {
        //                         wait();
        //                 } catch(Exception e) {
        //
        //                 }
        //         }
        //         System.out.println(Thread.currentThread().getName() + " " +currentVal++);
        //
        //         notify();
        // }
}

class EvenPrinter implements Runnable {

        Printer p;

        EvenPrinter(Printer p){
                this.p = p;
        }
        public void run() {
                for (int i = 0; i <10 ; i++ ) {
                        synchronized(Printer.class){
                                if(p.currentVal % 2 == 1){
                                        try{
                                                p.wait();
                                        }catch(Exception e){

                                        }
                                }
                                System.out.println(Thread.currentThread().getName() + " " + p.currentVal++);
                                p.notify();

                        }
                }
        }
}

class OddPrinter implements Runnable {
        Printer p;

        OddPrinter(Printer p){
                this.p = p;
        }

        public void run() {
                for (int i = 0; i <10 ; i++ ) {
                        synchronized(Printer.class){
                                if(p.currentVal % 2 == 0){
                                        try{
                                                p.wait();
                                        }catch(Exception e){

                                        }
                                }
                                System.out.println(Thread.currentThread().getName() + " " + p.currentVal++);
                                p.notify();
                        }

                }
        }
}

public class OddEven {
        public static void main(String[] args) {

                Printer p = new Printer();

                Thread t1 = new Thread(new EvenPrinter(p));
                Thread t2 = new Thread(new OddPrinter(p));

                t1.start();
                t2.start();
        }
}
