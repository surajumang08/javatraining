/*

*/

package multithreading;
class Local implements Runnable {

        private Local ll;

        public void set(Local ll){
                this.ll = ll;
        }
        public void run(){
                synchronized(ll){
                        for (int i = 0; i<20 ; i++ ) {
                                System.out.println(Thread.currentThread().getName() + " " + i);
                        }
                }
        }
}

public class Join {

        public static void main(String[] args) {
                Local l = new Local();

                Thread t1 = new Thread(l);

                t1.start();

                try{
                        t1.join();
                }catch(Exception e){

                }

                for (int i = 0; i<20 ; i++ ) {
                        System.out.println(Thread.currentThread().getName() + " " + i);
                }
        }
}
