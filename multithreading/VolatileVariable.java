package multithreading;

class Common {
        public volatile int a = 0;
}
class First implements Runnable {
        Common c;

        First(Common c){
                this.c = c;
        }
        public void run(){

                int temp = c.a;

                try {
                        Thread.sleep(1000);
                } catch(Exception e) {

                }
                System.out.println(temp + " " + c.a);
        }
}
class Second implements Runnable {
        Common c;

        Second(Common c){
                this.c = c;
        }
        public void run(){

                for (int i =0;  i < 5 ; i++ ) {
                        System.out.println("Second");
                        c.a++;
                }
        }
}

public class VolatileVariable {

        public static void main(String[] args) {
                Common c = new Common();
                Thread t1 = new Thread(new First(c));
                Thread t2 = new Thread(new Second(c));

                t1.start();
                t2.start();


        }
}
