/*

*/
package serialization;
import classesandinterfaces.*;
import java.io.*;

public class ParentNotSerialized extends Apple implements Serializable {

    String firstname = "Suraj";
     static transient Apple a;

    ParentNotSerialized(String firstname) {
        System.out.println("Current class");
        this.firstname = firstname;
        this.a = new Apple("Hello");
    }

    public static void main(String[] args) {

        try {
            ObjectOutputStream os = new ObjectOutputStream(
                new FileOutputStream("Ser.txt"));
            ParentNotSerialized p = new ParentNotSerialized("Rahul");
            System.out.println(p.a.getName());
            os.writeObject(p);
            os.close();

            System.out.println("After Deserialization");

            ObjectInputStream is = new ObjectInputStream(
                new FileInputStream("Ser.txt"));

            ParentNotSerialized d = (ParentNotSerialized)is.readObject();
            System.out.println(d.firstname);
            System.out.println(d.a.getName());
        }
        catch (Exception e) {e.printStackTrace();}
    }
}
