

package serialization;
import classesandinterfaces.*;
import java.io.*;

public class OverrideDefault implements Serializable{
    public String name ;
    transient Apple a;    //Apple is not Serializable.

    OverrideDefault(String name, Apple a) {
        this.name = name;
        this.a = a;
    }

    private void readObject(ObjectInputStream i) throws java.io.IOException, ClassNotFoundException {
        i.defaultReadObject();
        a = new Apple ((String)i.readUTF());
    }

    private void writeObject(ObjectOutputStream o) throws java.io.IOException {
        o.defaultWriteObject();
        o.writeUTF(a.toString());
    }
}

class Test {

    public static void main(String[] args) {
        OverrideDefault oo = new OverrideDefault("Suraj", new Apple("HelloWorld"));

        try{
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("Apple.ser"));
            o.writeObject(oo);
        }catch(Exception e){
            e.printStackTrace();
        }

        // deserialize
        try{
            ObjectInputStream i = new ObjectInputStream(new FileInputStream("Apple.ser"));

            OverrideDefault o  = (OverrideDefault) i.readObject();
            System.out.println(o.name + " " + o.a);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
