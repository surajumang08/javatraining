package serialization;

import java.io.*;

public class Car extends Vehicle implements Serializable {

}

class Vehicle {

}

class Wheel {
    public void show(){
        System.out.println("Wheel");
    }
}

class Ford extends Car {

}

class Dodge extends Car {
    transient Wheel w = new Wheel();
    String name = "Suraj";

    public static void main(String[] args) {
        try {
            ObjectOutputStream os = new ObjectOutputStream(
                new FileOutputStream("Ser.txt"));
            os.writeObject(new Dodge());
            os.close();

            ObjectInputStream is = new ObjectInputStream(
                new FileInputStream("Ser.txt"));

            Dodge d = (Dodge)is.readObject();
            System.out.println(d.name);
        }
        catch (Exception e) {e.printStackTrace();}
    }
}
