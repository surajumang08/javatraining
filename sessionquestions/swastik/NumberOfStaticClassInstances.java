/*
    This code tries to test how many instances of a class can be created
    which is declared as a static nested class.

    Things I Know : A static nested class is just like any other Outer
        class. The only differnce is that It is wrapped inside another
        class so that it may not be visible outside the class in which it is
        defined

    Assumption :
*/

/*
    Also need to know what modifiers can be applied to a member class which
    is static.
    And if there is any limit then Why does that limit exist?
*/

class Outer {

    private static class Inner {
        private int a = 45;
    }
}

public class NumberOfStaticClassInstances {

}
