/*
    This shows hoe int and Integer are represented differently.
*/

// public class Autoboxing {
//     public static void main(String[] args) {
//
//
//         method1(45);
//         int a =0;
//         System.out.println("" + a++ + a++);
//
//         // do {
//         //   main(new String[]{"djsf"});
//         // }while(true);
//         // method1(new Integer(45));
//     }
//
//     // public static void method1(int a){
//     //     System.out.println("int");
//     // }
//     public static void method1(Integer args) {
//         System.out.println("Integer");
//     }
// }

class Network {
    Network(int x, Network n) {
        id = x;
        p = this;
        if(n != null) p = n;
    }

    int id;
    Network p;

    public static void main(String[] args) {

        Network n1 = new Network(1, null);
        n1.go(n1);
    }

    void go(Network n1) {
        Network n2 = new Network(2, n1);
        Network n3 = new Network(3, n2);
        System.out.println(n3.p.p.id);
    }
}
