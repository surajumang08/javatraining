package sessionquestions.swastik;

import classesandinterfaces.*;
/*
    The hierarchy is : Fruit is the parent, Apple and Mango both extend Fruit.
*/
public class TwoSubtypesAsArgument {

    public static void main(String[] args) {
      final TwoSubtypesAsArgument t = new TwoSubtypesAsArgument();
         //method1(new Fruit());

         /*
            This call must be resolved at compile time only.
            But since the object being passed can be assigned to both
            the methods and the actual type of the passed parameter (new Apple)
            is known only at run time how is the compiler able to resolve the call.

            I think it assumes the type of (new Apple()) to be Apple.
            What is the rule followed by the compiler to resolve the call.
         */

         method1(new Apple());

         Apple a = new Apple();
         /*
            This call makes sense because it will search for a method which
            takes a parameter of type Apple and since Fruit is also an Apple
            it will be assigned to Fruit's reference if there is no method
            accepting Apple's reference.
         */
         method1(a);

    }

    /*
        The compiler is able to distinguish the parameters passed
        to the methods even though they belong to the same hierarchy.

        Need more clarity on reference types and their relation with the java
        compiler.
    */

    static void  method1(Fruit f){
        System.out.println("Fruit");
    }
    /*
        The calls in main will be okay even if the below method is not present.
    */
    static void  method1(Apple a){
        System.out.println("Apple");
    }
}
