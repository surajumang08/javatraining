/*
    If the method has a return type then it must return a value in case it
    explicitly throws only Checked exception
    so the return statement must be reachable from the compiler's
    perspective.

    1   Writing return after throw will generate an error.
    2   Writing return only in selected catch blocks will also be CTE.
    3   Writing a return in finally is Okay.
    4   If there is a return in finally then the catch blocks aren't required
        to return anything.
    5   But if the catch blocks are throwing some exception and the finally
        contains a return statement then those exception will not be thrown.
*/
package sessionquestions.swastik;

public class TryWithReturn {

    public int call() throws java.io.FileNotFoundException {
        int a = 9;
        try{
            throw new java.io.IOException();
        }catch(java.io.FileNotFoundException e){
            /*
                Even though the exception thrown in the try block is caught
                by this catch block, and it is throwing an exception itself
                but since the finally has a return statement this exception
                won't be thrown.

                Even though this exception will not be thrown the compiler
                wants us to declare that the following may be thrown.
            */
            System.out.println("afbk");
            //throw new java.io.IOException();
            //return ++a;
        }
        // catch(Exception e){
        //     System.out.println(a);
        //     //return ++a;
        // }
        finally{
            return ++a;
        }
        // Unreachable statement.
        //System.out.println("Hello");
    }
    public static void main(String[] args) throws java.io.FileNotFoundException{

        System.out.println(new TryWithReturn().call());
    }
}
