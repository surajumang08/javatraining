
package sessionquestions.swastik;
import classesandinterfaces.*;

public class TypeMatchingAtCompileTime {
    int a = 500, b = 4520;
    public TypeMatchingAtCompileTime(){

    }

    public static void main(String[] args) {
        TypeMatchingAtCompileTime t =
                new TypeMatchingAtCompileTime();

        t.call( new Alphonse());
    }

    public void call (Fruit f){
        System.out.println("Fruit's version");
    }

    public void call(Mango m) {
        System.out.println("Mango's version");
    }
}
