/*

*/
package sessionquestions.swastik;

class Parent {

  public int s = 452;

  public void show(Child child) {
    int s = 9999999;
    System.out.println("Printing data members...");
    System.out.println(s);
    System.out.println(this.s);
    System.out.println(child.s);
    System.out.println(child.p);
    System.out.println("Calling display...");
    display();
    this.display();
    child.display();
  }

  public void display() {
    System.out.println("Parent");
  }
}

class Child extends Parent {

  public int s = 6234;
  public int p = 123;

  public void display() {
    System.out.println("Child");
  }
}

public class InheritButNotOverride {

  public static void main(String[] args) {
    Child a = new Child();
    Child b = new Child();
    a.show(a);
    System.out.println();
    a.show(b);
  }
}
