/*
    It is already known that if no constructor is provided by the user
    then a default no arg constructor is provided which calls the no arg
    constructor of superclass using super().

    Need to use the decompiler to find out what the compiler inserts when
    no constructor is provided.
    And what is produced by the compiler when a no arg constructor with
    empty body is provided.

*/
