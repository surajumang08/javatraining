/*

*/
package sessionquestions.guru;

public class PrimitiveTypeOverloadResolution {

    public void call(byte a){
        System.out.println("Byte");
    }

    public void call(short a){
        System.out.println("Short");
    }

    public void call(int a){
        System.out.println("int");
    }

    public void call(long a){
        System.out.println("long");
    }

    public static void main(String[] args) {
        PrimitiveTypeOverloadResolution p = new
        PrimitiveTypeOverloadResolution();

        p.call(45);  //  int
        p.call(45l);    //long
        p.call((byte)45);   //Byte
        p.call((short)45);  //short
        p.call((long)45);   //long
    }
}
