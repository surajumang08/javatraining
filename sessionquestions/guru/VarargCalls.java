/*

*/
package sessionquestions.guru;

public class VarargCalls {

    VarargCalls(int a){
        System.out.println("int");
    }

    VarargCalls(int ... a){
        System.out.println("Varargs");
    }

    public void call(int a, int b){
        System.out.println("Both integer");
    }
    
    public void call(int ... a){
        System.out.println("VarargCalls");
    }

    public static void main(String[] args) {
        VarargCalls v = new VarargCalls();
        v.call(2,45);
        v.call();
        v.call(45, 52, 63);
    }
}
