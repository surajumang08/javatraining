

package sessionquestions.guru;
import java.io.*;

public class GettersInSerialization implements Serializable {

    private static final long serialVersionUID = 42l;
    private String firstName;
    private String lastName;
    private String clan;

    GettersInSerialization(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFName (){
        //System.out.println("giving first name");
        return firstName;
    }

    public String getLName(){
        //System.out.println("giving last name");
        return lastName;
    }

    public String getClan() {
        return clan;
    }

}
