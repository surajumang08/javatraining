
package sessionquestions.guru;
import java.io.*;

public class TestGetter {
    public static void main(String[] args) {

        TestGetter t = new TestGetter();
        if(args[0].equals("serial"))
            t.serial();
        else
            t.deserial();


    }

    public void serial(){
        GettersInSerialization g = new GettersInSerialization("Harry", "Potter");
        // Serialize.
        try{
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("Fruit.ser"));
            o.writeObject(g);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void deserial () {
        //deserialize
        GettersInSerialization g =new GettersInSerialization("Helo", "Worlsd");
        System.out.println("desiral called");
        try{
            ObjectInputStream i = new ObjectInputStream(new FileInputStream("Fruit.ser"));

            GettersInSerialization o  = (GettersInSerialization) i.readObject();
            System.out.println(o.getFName() + " " + o.getLName());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
