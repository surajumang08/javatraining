/*
    This program shows overloaded call resolution.
*/
package sessionquestions.guru;

public class MixedCall {

    // public void call(long l, long m){
    //     System.out.println("long long");
    // }

    public void call(Long l, long m){
        System.out.println("Long long");
    }

    public void call(long l, Long m){
        System.out.println("long Long");
    }

    public void call(Long l, int m){
        System.out.println("Long int");
    }

    public static void main(String[] args) {

        MixedCall m = new MixedCall();
        /*
            If the first call with both primitive long is not present then
            this will be the output.
        */
        m.call(1l, 45);// calls wrapper with long.
        m.call(12, 12l);   //calls long with wrapper.

    }
}
