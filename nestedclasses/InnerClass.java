/*

*/

package nestedclasses;

class CPU {
    double price;

    public double getProcessorCache(){
        return new Processor().getCache();
    }
    private class Processor{
        double cores;
        String manufacturer;
        double getCache(){
            return 4.3;
        }
    }
    protected class RAM{
        double memory;
        String manufacturer;
        double getClockSpeed(){
            return 5.5;
        }
    }
}
public class InnerClass {
    public static void main(String[] args) {
        CPU cpu = new CPU();
        //CPU.Processor processor = cpu.new Processor();
        CPU.RAM ram = cpu.new RAM();
        System.out.println("Processor Cache = " + cpu.getProcessorCache());
        System.out.println("Ram Clock speed = " + ram.getClockSpeed());
    }
}
