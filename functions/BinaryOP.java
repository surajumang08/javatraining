
package functions;

import java.util.function.*;

public class BinaryOP{

	public static void main(String args[]) {
		UseMeToo a=new UseMeToo(5);
		UseMeToo b=new UseMeToo(5);
		BinaryOperator<UseMeToo> adder = (p,q) -> {return new UseMeToo(p.a + q.a);};
		UseMeToo d = adder.apply(a,b);
		System.out.println(d.a);
	}
}
class UseMeToo {
	int a;
	UseMeToo(){

    }
	UseMeToo(int i){
		a=i;
	}
}
