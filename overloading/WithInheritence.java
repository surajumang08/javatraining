/*
    At compile time the compiler tries to find a matching method
    which accepts the exact parameter type with which the method has
    been called.

*/

class Apple extends Fruit {
    void meth(Fruit f) {
        System.out.println("Fruit Child");
    }

}

class Fruit {
    //Overloading.

    // void meth(Apple a) {
    //     System.out.println("Apple");
    // }
    void meth(Fruit f) {
        System.out.println("Fruit");
    }
}

public class WithInheritence {

    public static void main(String[] args) {
        Fruit f = new Apple();

        //f.meth(new Fruit());
        f.meth(new Apple());

    }
}
