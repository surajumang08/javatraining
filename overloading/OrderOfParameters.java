/*
    The Neccessary and suffucient condition for overloading a method is that the
    number or type or both of the parameters passed to the functions must be different.

    1 : The return type may or may not change. A function having same number and type of
        parameters but with different return types are not Overloaded.

    2   : To test if two functions having the same number and type of parameters
        but accepting them in different order. Will they be called Overloaded.
        My Assumption [ NO! they won't be overloaded.]
        Actual [It works they both are Overloaded].

*/

public class OrderOfParameters {
    public void callmethod(String val, int a){
        System.out.println("Variant One");
    }

    public void callmethod(int a, String val){
        System.out.println("Variant Two");
    }

    public static void main(String[] args) {
        OrderOfParameters o = new OrderOfParameters();
        o.callmethod(12, "Suraj");
        o.callmethod("Suraj", 12);
    }
}
