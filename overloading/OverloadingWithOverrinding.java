/*
    Overloading Can be coupled with overriding.
    When A class inherits a method and provides an overloaded variant of the same
    method.

*/

class Parent {
    void callmethod(String val, int a){
        System.out.println("Parent's version");
    }
}

public class OverloadingWithOverrinding extends Parent {
    /*
        Overriding doesn't allow the access specifier to be more restrictive than
        the parent's version but the below method is being Overloaded
        and NOT Overridden.
        Access specifier can be anything.
    */
    private void callmethod(int a, String val) {
        System.out.println("Child's Overloaded version");
        System.out.println(a + " " + val);
    }

    /*
        Overridden version but using a less restrictive access modifier
        than the parent's version.
        default to protected.
    */
    protected void callmethod(String val, int a) {
        System.out.println("Overridden version");
    }


    public static void main(String[] args) {
        OverloadingWithOverrinding o = new OverloadingWithOverrinding();
        o.callmethod("Suraj", 12); // Overridden version
        o.callmethod(12, "Suraj"); // overloaded version
    }
}
