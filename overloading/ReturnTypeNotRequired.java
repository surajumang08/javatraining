package overloading;
public class ReturnTypeNotRequired {
    int call (int a){
        return a;
    }
    float call(int a, int c){
        String b;
        return a;
    }
    public static void main(String[] args) {
        new ReturnTypeNotRequired().call(45);
    }
}
